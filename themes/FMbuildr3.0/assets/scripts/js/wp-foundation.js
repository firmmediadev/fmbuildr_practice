/*
These functions make sure WordPress
and Foundation play nice together.
*/
var window = jQuery(window);
jQuery(document).ready(function() {	
	initSwatch();
	initHpVideo();
	initCheckVideo();
	initPreloaderScreen();
	initNavigation();
	initParallax();
	initJsEssentials();
	initCurtainOffCanvas();
	initDebugin();
	initMobileExpandr();
	initGalleries();
	initScroller();
	initFlickity();
	initBrowserExposure();
});


function initFlickity(){	
	
}	


function initBrowserExposure() {
	 // {{{ win-safari hacks, scratch this,
  // let's just expose platform/browser to css
  (function()
  {
    var uaMatch = '', prefix = '';

    if (navigator.userAgent.match(/Windows/))
    {
      jQuery('html').addClass('x-win');
    }
    else if (navigator.userAgent.match(/Mac OS X/))
    {
      jQuery('html').addClass('x-mac');
    }
    else if (navigator.userAgent.match(/X11/))
    {
      jQuery('html').addClass('x-x11');
    }

    // browser
    if (navigator.userAgent.match(/Chrome/))
    {
      uaMatch = ' Chrome/';
      prefix = 'x-chrome';
    }
    else if (navigator.userAgent.match(/Safari/))
    {
      uaMatch = ' Version/';
      prefix = 'x-safari';
    }
    else if (navigator.userAgent.match(/Firefox/))
    {
      uaMatch = ' Firefox/';
      prefix = 'x-firefox';
    }
    else if (navigator.userAgent.match(/MSIE/))
    {
      uaMatch = ' MSIE ';
      prefix = 'x-msie';
    }
    // add result preifx as browser class
    if (prefix)
    {
      jQuery('html').addClass(prefix);

      // get major and minor versions
      // reduce, reuse, recycle
      uaMatch = new RegExp(uaMatch+'(\\d+)\.(\\d+)');
      var uaMatch = navigator.userAgent.match(uaMatch);
      if (uaMatch && uaMatch[1])
      {
        // set major only version
        jQuery('html').addClass(prefix+'-'+uaMatch[1]);
        // set major + minor versions
        jQuery('html').addClass(prefix+'-'+uaMatch[1]+'-'+uaMatch[2]);
      }
    }
  })();
  // }}}
}


function initScroller(){	
	jQuery('.scrollink').click(function() { 
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		var target = jQuery(this.hash); 
		target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
		if (target.length) {
			jQuery('html,body').animate({
			scrollTop: target.offset().top-210
			}, 1000);				
			return false;
			}
		}
	});
}


function initGalleries(){
	jQuery('.imgGalItem').click(function() {
			
		var link = jQuery(this).attr('data-link');
		var rel  = jQuery(this).attr('rel');
		var counts = jQuery(this).attr('data-groups');			
		var newContent = '<a href="'+link+'" rel="galSlideCell'+rel+'" data-fancybox="group'+counts+'" data-caption="*Individual Results May Vary"><img src="'+link+'"></a> ';
		
		jQuery(".photoGallery"+rel).removeClass("is-active");
		jQuery(this).addClass("is-active");		
				
		jQuery(".galSlideCell"+rel).stop().css('opacity', '0').html(function () {
		 		return newContent;
		    }).animate({
		        opacity: 1
		    },500);			
		event.preventDefault();	
		return false;
	});
	
	jQuery( "#listView" ).click(function() {
      event.preventDefault();		  
	  jQuery(this).addClass("active");
	  jQuery( "#gridView" ).removeClass("active"); 	  
	  jQuery( ".imgWrapSub" ).removeClass( "toggleHide" );
	});
	
	jQuery( "#gridView" ).click(function() {
	  event.preventDefault();	
	  jQuery(this).addClass("active");
	  jQuery( "#listView" ).removeClass("active"); 
	  jQuery( ".imgWrapSub" ).addClass( "toggleHide" );
	});
	
	
	jQuery('select#mobile-sorting').on('change', function() {
	  window.location.replace(this.value);
	});
	
}



function initMobileExpandr() {
	//alert('helo joshua');
	var expandTar = jQuery('.mobileExpand'),
		ehmax = '',
		expandTarBtn = '<a class="expandBtn"><span class="showMe">Expand</span><span class="hideMe">Collapse</span> <i class="rotateMe none fa fa-angle-double-right" aria-hidden="true"></i></a>';
	
	expandTar.wrapInner('<div class="expandrContent"></div>');
	expandTar.append(expandTarBtn);

	jQuery('.expandrContent').each( function(){
		ehmax = jQuery(this).parent().data('eh-max');
		jQuery(this).find('.expandrContent').css('max-height', ehmax);		
		jQuery(this).css('max-height', ehmax );
	});
	
	jQuery('.expandBtn').on('click', function(){
		jQuery(this).find('.rotateMe').toggleClass('none down');
		jQuery(this).find('span').toggleClass('showMe hideMe');
		jQuery(this).parent().find('.expandrContent').toggleClass('expanded');
		jQuery(this).toggleClass('expanded');	
	});
		
}


function initCurtainOffCanvas() {
	jQuery('[data-curtain-menu-button]').click(function(){
		jQuery('body').toggleClass('curtain-menu-open');
	})
}

function initParallax(){	
	
    jQuery(window).on('load scroll', function () {
		var scrolled =  jQuery(this).scrollTop(),
			xpos = 50,
			ypos = 50,
			bgsize = 'cover',
			scrollBottom = jQuery(this).scrollTop() + jQuery(this).height();      	
    	
    	jQuery('.tagline').each(function(){  
	    	var selectors = jQuery(this);
	    	var theBottom = selectors.position().top + selectors.outerHeight(true);	
	    	if(scrolled <= theBottom) {
	    		jQuery('.tagline').css({
	        		'transform': 'translate3d(0, ' + -(scrolled * 0.25) + 'px, 0)', 
					'opacity': 1 - scrolled / 400 // fade out at 400px from top
	    		}); 
	    	}    	  
     	});
        	
        /* Uses the default class name 'banner-parallax' on the frontpage banner, use data-speed="0.2"(20% scroll rate use decimals)  */                
        jQuery('.banner-parallax').each(function(){ 
	        	var speed = jQuery(this).attr('data-speed');
	        	var position = jQuery(this);
	        	var bottom = position.position().top + position.outerHeight(true);	  		        	
	        
	        	if(scrolled <= bottom) {	
	        		jQuery(this).css('transform', 'translate3d(0, ' + (scrolled  * speed) + 'px, 0)');
	        	}        	     	
        });
        
        
       	/* Use the class name 'parallax', use data-speed="0.2"(20% scroll rate use decimals)  */
	   	jQuery('.parallax').each(function(){ 
        	var speed = jQuery(this).attr('data-speed'),
        		xposval = jQuery(this).attr('data-xpos'),
        		yposval = jQuery(this).attr('data-ypos'),
        		bgsizeval = jQuery(this).attr('data-bg-size'),
        		parent = jQuery(this).parent(),
        		position = parent.position(),
        		bottom = parent.position().top + parent.outerHeight(true);
			if (xposval) {
	        	xpos = xposval;
        	}
        	if (yposval){
	        	ypos = yposval;
        	} 
        	if (bgsizeval){
	        	bgsize = bgsizeval;
        	}
        	if(!speed){ speed = 0.15; }
			if(scrolled <= bottom && scrollBottom > position.top ) {        		        	
        		jQuery(this).css({
	        		'background-position': xpos + '% calc(' + ypos +'% + ' + ((scrolled  - position.top ) * speed) + 'px)',
	        		'background-size': bgsize
	        	}); 
        		
        	}
	        	      	
        });
        
        
    });	    
}


function initJsEssentials() {
	    // Remove empty P tags created by WP inside of Accordion and Orbit
    jQuery('.accordion p:empty, .orbit p:empty').remove();

	// Adds Flex Video to YouTube and Vimeo Embeds
	jQuery('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
		if ( jQuery(this).innerWidth() / jQuery(this).innerHeight() > 1.5 ) {
		  jQuery(this).wrap("<div class='widescreen responsive-embed'/>");
		} else {
		  jQuery(this).wrap("<div class='responsive-embed'/>");
		}
	});
	
	jQuery('.main .grid-x_wrapper').removeClass('grid-x_wrapper').find('.gform_body .gform_fields').addClass("grid-x grid-padding-x");
	
	//jQuery('.main .gform_wrapper .gform_body .gform_fields').addClass("grid-x");
	jQuery('.main .gfield.hidden_label .gfield_label').remove();
	// jQuery('.main .gform_footer').addClass('cell small-12 medium-auto'); 	
	
	jQuery('#additional-footer .grid-x_wrapper').removeClass('grid-x_wrapper').find('.gform_body').addClass("cell small-12 medium-9");
	jQuery('#additional-footer .gfield.hidden_label .gfield_label').remove();
	jQuery('#additional-footer .gform_footer').addClass('cell small-12 medium-auto'); 
	
	jQuery('.stars').html( "<i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i><i class='fa fa-star'></i>" );
	
	// This fixes the flash bug on the navigation
	jQuery('.lower-bar').removeClass('wait-js');
	jQuery('.sticky-wrapper-navigation').removeClass('wait-js');
	
}

function initPreloaderScreen() {
	setTimeout(function(){
		jQuery('.home #loader').addClass('animated zoomOut');		
	}, 800);
	setTimeout(function(){		
		jQuery('.home #loader-wrapper').fadeOut('slow');	
		jQuery('.home .off-canvas-wrapper').css('opacity', 1);
	}, 1200);
	
/*
	
	USE THIS AS AN EXAMPLE FOR FUTURE BUILD
	
	var fpBannerLogo = jQuery('.slide-logo');
	var fpButton = jQuery('.fp-banner-link');
	var fpfixedlogo = jQuery('.box .button');
	
	if(fpBannerLogo){
		fpBannerLogo.css('opacity',0);
		setTimeout(function(){
			fpBannerLogo.css('opacity',1);
			fpBannerLogo.addClass('animated zoomIn');			 
		}, 1400);
	}
	if(fpfixedlogo) {
		setTimeout(function(){ 
			fpfixedlogo.addClass('animated fadeIn');
		}, 1400);
	}
	
	if(fpButton) {
		fpButton.css('opacity',0);
		setTimeout(function(){
				fpButton.css('opacity',1);			
			fpButton.addClass('animated fadeIn');
			
		}, 1300);
	}
*/
	initSubmenus();
}

function initSubmenus() {
	jQuery('.top-bar #menu-topnav-1').css('opacity',1);	
	setTimeout(function(){
		jQuery('.top-bar .fixedlogo').css('display','block').addClass('animated fadeInLeft');
	}, 800);	
}


function initNavigation() {
	jQuery('.sticky-wrapper-navigation').on('sticky.zf.stuckto:top', function(){
	  	jQuery(this).addClass('shrink');
	}).on('sticky.zf.unstuckfrom:top', function(){
	  	jQuery(this).removeClass('shrink');
	})
}


var navigator;
var DocumentTouch;
// VIDEO FOR FRONT PAGE
function initHpVideo() {		
			var isTouchDevice = /Windows Phone/.test(navigator.userAgent) || ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch;
		
	jQuery('.banner-wrapper .video').each(function() {
		var holder = jQuery(this);
		var video = holder.find('video');
		var isDesktop = jQuery(document).width() < 1025 ? true : false; 
		
		if (isTouchDevice && isDesktop) {
			video.remove();
			jQuery(".banner-wrapper .video").addClass("mobile");
			jQuery(".banner-wrapper .video").removeClass("desktop");
		} else {
			jQuery(".banner-wrapper .video").addClass("desktop");
		}
	});
}

function initCheckVideo(){
var vid = document.getElementById("fpVideo");

	if (vid) {
		vid.onloadedmetadata = function() {   
		    jQuery('.loader').fadeOut('slow');
		};
	}
}


// builds color swatches for style guide
function rgb2hex(rgb) {
    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
    }
    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

function initSwatch() {
	var swatchcolor = '#eee';
	var swatchbgc ="rgb(238,238,238)";
	var swatchhex = '#eee';
	var swatchTxt = '';
	var swatchInfo = '';
	var yus = [
		'primary',
		'secondary',
		'warning',
		'success',
		'alert',
	];
	jQuery('.swatch-block') .each(function(sw){
		swatchcolor = jQuery(this).attr("data-swatch");
		//console.log(swatchcolor);
		if ( 
			swatchcolor === 'primary' || 
			swatchcolor === 'secondary' || 
			swatchcolor === 'success' || 
			swatchcolor === 'warning' || 
			swatchcolor === 'alert'
			) {
			jQuery(this).wrapInner('<div class="cell auto swatch-info ">','</div>');
			jQuery('<div class="cell swatch"></div>').prependTo(this);
			jQuery(this).find('.swatch').addClass(swatchcolor+'-swatch');
			swatchbgc = jQuery(this).find('.swatch').css('background-color');
			swatchhex = rgb2hex(swatchbgc);
			swatchTxt = jQuery('<span>' + swatchhex + '</span>');
			swatchInfo = jQuery(this).find('.swatch-info');
			jQuery(swatchTxt).prependTo(swatchInfo);
			jQuery(this).wrapInner('<div class="grid-x ">','</div>');			
		} else {
			jQuery('<span>' + swatchcolor + '</span>').prependTo(this).children('p');
			jQuery(this).wrapInner('<div class="cell auto swatch-info ">','</div>');
			jQuery('<div class="cell swatch" style="background-color: ' + swatchcolor +';"></div>').prependTo(this);
			jQuery(this).wrapInner('<div class="grid-x ">','</div>');			
		}
	});

}
var Foundation;
// used to console log screen breakpoint changes while developing themes
function initDebugin() {
	var currentBreakpoint = Foundation.MediaQuery.current;
	//console.log('the current breakpoint is ' + currentBreakpoint);
	jQuery(window).resize(function() {
		var currentBreakpoint = Foundation.MediaQuery.current;
		
		if(currentBreakpoint == 'small' || currentBreakpoint == 'smedium'){
			jQuery('.specialtySlide').addClass('fadeIn').removeClass('fadeOut');
		}
	});

}


//	Animations v1.1, Joe Mottershaw (hellojd)
//	https://github.com/hellojd/animations / https://github.com/Sananes/animations

//	==================================================

//	Visible, Sam Sehnert, samatdf, TeamDF
//	https://github.com/teamdf/jquery-visible/
//	==================================================

	(function($){
		$.fn.visible = function(partial,hidden,direction) {
			var $t				= jQuery(this).eq(0),
				t				= $t.get(0),
				$w				= jQuery(window),
				viewTop			= $w.scrollTop(),
				viewBottom		= viewTop + $w.height(),
				viewLeft		= $w.scrollLeft(),
				viewRight		= viewLeft + $w.width(),
				_top			= $t.offset().top,
				_bottom			= _top + $t.height(),
				_left			= $t.offset().left,
				_right			= _left + $t.width(),
				compareTop		= partial === true ? _bottom : _top,
				compareBottom	= partial === true ? _top : _bottom,
				compareLeft		= partial === true ? _right : _left,
				compareRight	= partial === true ? _left : _right,
				clientSize		= hidden === true ? t.offsetWidth * t.offsetHeight : true;
				direction		= (direction) ? direction : 'both';

			if(direction === 'both')
				return !!clientSize && ((compareBottom <= viewBottom) && (compareTop >= viewTop)) && ((compareRight <= viewRight) && (compareLeft >= viewLeft));
			else if(direction === 'vertical')
				return !!clientSize && ((compareBottom <= viewBottom) && (compareTop >= viewTop));
			else if(direction === 'horizontal')
				return !!clientSize && ((compareRight <= viewRight) && (compareLeft >= viewLeft));
		};
		

		$.fn.fireAnimations = function(options) {
			function animate() {
				if (jQuery(window).width() >= 960) {
					jQuery('.animate').each(function(i, elem) {
							elem = jQuery(elem);
							var	type = jQuery(this).attr('data-anim-type'),
							delay = jQuery(this).attr('data-anim-delay');

						if (elem.visible(true)) {
							setTimeout(function() {
								elem.addClass(type);
							}, delay);
						} 
					});
				} else {
					jQuery('.animate').each(function(i, elem) {
							elem = jQuery(elem);
						var	type = jQuery(this).attr('data-anim-type'),
							delay = jQuery(this).attr('data-anim-delay');

							setTimeout(function() {
								elem.addClass(type);
							}, delay);
					});
				}
			}

			jQuery(document).ready(function() {
				jQuery('html').removeClass('no-js').addClass('js');

				animate();
			});

			jQuery(window).scroll(function() {
				animate();

				if (jQuery(window).scrollTop() + jQuery(window).height() == jQuery(document).height()) {
					animate();
				}
			});
		};

		jQuery('.animate').fireAnimations();

	})(jQuery);
		
	var	triggerClasses = 'flash strobe shake bounce tada wave spin pullback wobble pulse pulsate heartbeat panic explode';
		
	var	classesArray = [];
		classesArray = triggerClasses.split(' ');

	var	classAmount = classesArray.length;
	var type;
	
	function randomClass() {
		var	random = Math.ceil(Math.random() * classAmount);

		type = classesArray[random];

		return type;
	}

	function triggerOnce(target, type) {
		if (type == 'random') {
			type = randomClass();
		}

		jQuery(target).removeClass('trigger infinite ' + triggerClasses).addClass('trigger').addClass(type).one('webkitAnimationEnd oAnimationEnd MSAnimationEnd animationend', function() {
			jQuery(this).removeClass('trigger infinite ' + triggerClasses);
		});
	}

	function triggerInfinite(target, type) {
		if (type == 'random') {
			type = randomClass();
		}

		jQuery(target).removeClass('trigger infinite ' + triggerClasses).addClass('trigger infinite').addClass(type).one('webkitAnimationEnd oAnimationEnd MSAnimationEnd animationend', function() {
			jQuery(this).removeClass('trigger infinite ' + triggerClasses);
		});
	}


/*
	jQuery(window).resize(function() {
		jQuery('.animate').fireAnimations();
	});
*/
