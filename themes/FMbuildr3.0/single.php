<?php 
/**
 * The template for displaying all single posts and attachments
 */

get_header(); ?>
		
<?php get_template_part('parts/components/component', 'banner');?>

<div class="content" id="content">

	<div class="grid-container">	

		<div class="inner-content grid-x grid-padding-x">
	
			<main class="main small-12 medium-8 large-8 cell" role="main">
			
			    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
			    	<?php get_template_part( 'parts/loops/loop', 'single' ); ?>
			    	
			    			<!--  load flexible content -->
					<?php get_template_part( 'parts/components/component', 'flexible-content' ); ?>
			    	
			    <?php endwhile; else : ?>
			
			   		<?php get_template_part( 'parts/contents/content', 'missing' ); ?>
	
			    <?php endif; ?>
	
			</main> <!-- end #main -->
	
			<?php get_sidebar(); ?>
	
		</div> <!-- end #inner-content -->
	
	</div> <!-- end #content -->

</div> <!-- end . grid-container -->

<?php get_footer(); ?>