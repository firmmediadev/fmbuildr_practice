<?php
function site_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way
    
    wp_enqueue_script( 'event-js', get_template_directory_uri() . '/assets/scripts/additional-scripts/jquery.event.move.js', array( 'jquery' ), '', true );
    wp_enqueue_script( 'twenty-js', get_template_directory_uri() . '/assets/scripts/additional-scripts/jquery.twentytwenty.js', array( 'jquery' ), '', true ); 
            
    // Adding scripts file in the footer
    wp_enqueue_script( 'site-js', get_template_directory_uri() . '/assets/scripts/scripts.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts/js'), true );

    // Register site fonts
    // currently loading from /assets/fonts/ testing performance via local vs external request
    //wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Average|Source+Sans+Pro:400,400i,600,700,900', array(), '', 'all' );
   
    // Register main stylesheet
    wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/styles/style.css', array(), filemtime(get_template_directory() . '/assets/styles/scss'), 'all' );

    // register fancyapp css 
    wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/assets/styles/css/jquery.fancybox.css', array('site-css'), filemtime(get_template_directory() . '/assets/styles/css/jquery.fancybox.css'), 'all' );
    
    // register flickity css 

    wp_enqueue_style( 'flickity-css', get_template_directory_uri() . '/assets/styles/css/flickity.css', array('site-css'), filemtime(get_template_directory() . '/assets/styles/css/flickity.css'), 'all' );

    // Comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }
}
add_action('wp_enqueue_scripts', 'site_scripts', 999);