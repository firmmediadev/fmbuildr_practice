<?php if(have_rows('modals_diag','options')): ?>
	
	<?php while (have_rows('modals_diag','options')): the_row(); ?>

		<?php 
			$title = get_sub_field('title','options');
			$background_image = get_sub_field('background_image', 'options');
			$content = get_sub_field('content','options');
			$size = get_sub_field('size','options');
			$unique_id = get_sub_field('unique_id','options'); 
			$text_color = get_sub_field('text_color','options');
		?>
		<div class="reveal <?=$size?>" data-animation-in="fade-in" data-animation-out="fade-out" id="<?=$unique_id;?>" data-reveal style="background-image:url(<?=$background_image['url'];?>); background-repeat: no-repeat; background-size: cover; color:<?=$text_color?>">
		  <div class="reveal-container">
			  <?=$content?>
		  </div>		  
		  <button class="close-button" data-close aria-label="Close modal" type="button">
		    <span aria-hidden="true" style="color:<?=$text_color?>;">&times;</span>
		  </button>	
		</div>
	<?php endwhile; ?>	
		
<?php endif; ?>