<?php // set up page banner acf fields 
$page_for_posts = get_option( 'page_for_posts', true );
if (is_home()){
	$post_id = 	$page_for_posts;
}  else {
	$post_id = $post->ID;
}
	
if(is_home() || is_archive() || is_404()):	
	$bannerBgImgs = get_field('banner_background_imgs','options');
	$bgColor = get_field('background_color','options');
else :
	$bannerBgImgs = get_field('banner_background_imgs');
	$bgColor = get_field('background_color');
endif;

$minHeight = get_field('banner_min_height');
$altTitle = get_field('alt_title', $post_id);
$geoTag = get_field('geo_tag' , $post_id);
$removeme =  array('Category:','Archives:');
 
if ($bannerBgImgs):
/* loop through the images in the banner background images gallery field and save them as an array 
** for use in our data-interchange background src.  
*/?>
	<?php $ctr = 0; foreach($bannerBgImgs  as $image ): $ctr++; ?>							 
		<?php $bannerimgs[$ctr] = $image['url']; ?>					
	<?php endforeach; ?>
<?php endif;?>

<?php $navigationType = get_theme_mod("fmbuildr_nav_type"); ?>

<?php if ( $navigationType == 'offcanvas_curtain') { $navStyleClass = 'navStyleCurtain'; } else { $navStyleClass = 'navStyleOffCanvas'; }?>
<style>
	.banner-content
	 {
		min-height: <?=$minHeight;?>
	}
	
</style>
<section id="page-banner" class="<?=$navStyleClass;?>">
	<div class="banner-bg banner-parallax  animated fadeIn" data-speed="0.3" <?php if ($bannerBgImgs):?>  style="background-size: cover; background-repeat: no-repeat;" data-interchange="[<?=$bannerimgs[3];?>, small], [<?=$bannerimgs[1];?>, xmedium], [<?=$bannerimgs[1];?>, large]" <?php else:?> style="background-color: <?=$bgColor;?>;" <?php endif;?>>
		<div class="grid-container">
			<div class="grid-x grid-padding-x align-middle banner-content">
				<div class="cell small-12 text-center">
					<?php if(is_home()): ?>
						<h1 class="page-title tagline"><?php if($altTitle) { echo $altTitle ;} else { echo get_the_title($post_id); }?> <?php if($geoTag) { echo'<small class="sub-header">' . $geoTag . '</small>';}?></h1>
					<?php elseif(is_archive()): ?>
						<h1 class="page-title tagline"><?php echo str_replace($removeme, "", get_the_archive_title()); ?></h1>
					<?php elseif(is_404()): ?>
						<h1 class="page-title tagline">Page not found</h1>
					<?php  else:?>
						<h1 class="page-title tagline"><?php if($altTitle) { echo $altTitle ;} else { the_title();}?> <?php if($geoTag) { echo'<small class="sub-header">' . $geoTag . '</small>';}?></h1>
					<?php endif; ?>
				</div>
			</div>
		</div>	
	</div>		
</section>