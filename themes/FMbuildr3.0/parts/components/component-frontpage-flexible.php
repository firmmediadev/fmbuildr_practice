<?php  

//If the main 'flexible fields' field has rows, make the rest of the page
if( have_rows('flexible_section_fields') ):

  // loop through the rows of data
    while ( have_rows('flexible_section_fields') ) : the_row();
		
		//If the row layout is a 'new_row', create the variables needed for making the fields
		if ( get_row_layout() == 'new_row'):
	
		$customID = get_sub_field( 'unique_id' );
		$customClass = get_sub_field('custom_classes');
		$add_bg_images = get_sub_field('add_bg_images');
		$bgImgs = get_sub_field('background_images');
		$add_column_padding = get_sub_field('add_column_padding');
		$fp_parallax_images = get_sub_field('fp_parallax_images');
		$data_attributes = get_sub_field('data_attributes');
		
		//If the row has background images, loop through the background images and create a variable for each image to store the image url
		$type_of_row = get_sub_field('type_of_row');
		 if ($bgImgs):
			 $ctr = 0; foreach($bgImgs  as $image ): $ctr++; 						 
				$bgImgs[$ctr] = $image['url']; 					
			endforeach; 
		 endif;	
		?>		
		<section id="<?=$customID;?>" class="fp-section-wrapper">
			<?php //If the section should have parallax, set the parallax to the type selected, and set the data-interchange (from Foundation) to the image and size set earlier ?>			
			<div class="fp-sections <?=$customClass?> <?php if($fp_parallax_images): echo "parallax"; endif;?>"  <?php if($add_bg_images): ?>data-interchange="[<?=$bgImgs[3];?>, small], [<?=$bgImgs[2];?>, medium], [<?=$bgImgs[1];?>, large]"<?php endif; ?> <?=$data_attributes?>>			 
				<div class="grid-x <?=$type_of_row?> <?=$customClass;?> <?=$add_column_padding?>" >
			<?php if( have_rows('columns') ):?>
		
				<?php while ( have_rows('columns') ) : the_row();?>			
					<?php 
						$colContent = get_sub_field('column_content');
						$colClasses = get_sub_field('custom_classes');
						$smCol = get_sub_field('sm_size');
						$mdCol = get_sub_field('md_size');
						$xmdCol = get_sub_field('xmd_size');
						$lgCol = get_sub_field('lg_size');
						$xlgCol = get_sub_field('xlg_size');
						$column_data_attribute = get_sub_field('column_data_attribute');
						$background_image_cell = get_sub_field('background_image_cell');
						$add_background_image = get_sub_field('add_background_image');
						if ($background_image_cell):
							$ctr = 0; 
							foreach($background_image_cell  as $image ): $ctr++; 						 
								$bgImgs[$ctr] = $image['url']; 					
							endforeach; 
						endif;	
					?>
						<div class="cell <?=$colClasses;?> small-<?=$smCol;?> medium-<?=$mdCol;?> xmedium-<?=$xmdCol;?> large-<?=$lgCol;?> xlarge-<?=$xlgCol?>"	<?php if($background_image_cell && $add_background_image): ?> data-interchange="[<?=$bgImgs[3]?>, small], [<?=$bgImgs[2]?>, medium], [<?=$bgImgs[1]?>, large]" style="height:100%" <?php endif; ?> <?=$column_data_attribute?>>
							<?=$colContent;?>	
						</div>			
						
				<?php endwhile;?>
						
			<?php endif;?>		
			</div>
			</div>		
		</section>
		
		<?php elseif ( get_row_layout() == 'image_slider' ):?>
		<?php 
		$sliderRowclasses = get_sub_field('img_slider_row_classes');
		$sliderclasses = get_sub_field('img_slider_classes');
		$slides = get_sub_field('slides');
		$dataAtts = get_sub_field('img_slider_data-attributes');
		?>
		<section class="flexibleSlider <?=$sliderRowclasses;?>">
		
			<div class="<?=$sliderclasses;?> carousel" <?=$dataAtts;?>>
				<?php foreach( $slides as $slide ): ?>
					<img class="slide" src="<?=$slide['url'];?>" alt="<?=$slide['alt'];?>">
				<?php endforeach;?>
			</div>

		</section>
		<?php endif;?>

		
	<?php endwhile; ?>	

<?php else : ?>

no layouts found

<?php  // no layouts found
endif; ?>

  