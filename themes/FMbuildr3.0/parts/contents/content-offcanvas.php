<?php
/**
 * The template part for displaying offcanvas content
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
?>

<?php 
	$mobileNavStyle = get_theme_mod("offcanvas_select_setting_id"); 
	$offCanvasTransition = get_theme_mod("offcanvas_transition_id");
?> 


<div class="off-canvas position-<?=$mobileNavStyle;?>" id="off-canvas" data-off-canvas data-transition="<?=$offCanvasTransition;?>">
	<?php 
		$phone = get_field('phone_number','option');
		$off_canvas_cta = get_field('off_canvas_cta','options');
		$off_canvas_cta_link = get_field('off_canvas_cta_link','options');
	?>
	<div class="grid-x offcanvas-phone-wrap">
		<div class="cell small-12 text-center offcanvas-phone-container">
				<a href="<?=$off_canvas_cta_link?>"><?php echo $off_canvas_cta; ?></a>				
		</div>
	</div>	
	<?php joints_topleft_nav(); ?>	
	<?php joints_off_canvas_nav(); ?>
</div>