<?php
/*
Plugin Name: Campaign Tracker for WordPress
Plugin URI: http://HelpForWP.com
Description: This plugin allows you to track marketing data like traffic referrer, Google campaign URL data and custom query string variables in your WordPress site and present this data along side form enquiries as they are received.
Version: 2.0
Author: HelpForWP.com
Author URI: http://HelpForWP.com
*/

global $_ct_plugin_name, $_ct_version, $_ct_home_url, $_ct_plugin_author, $_ct_messager, $_ct_menu_url;
global $_ct_forms_id_on_page_of_gravity_forms, $_ct_forms_id_on_page_of_ninja_forms, $_ct_forms_id_on_page_of_contact_form_7_forms;

$_ct_forms_id_on_page_of_gravity_forms = array();
$_ct_forms_id_on_page_of_ninja_forms = array();
$_ct_forms_id_on_page_of_contact_form_7_forms = array();

$_ct_plugin_name = 'Campaign Tracker for WordPress';
$_ct_version = '2.0';
$_ct_home_url = 'http://helpforwp.com';
$_ct_plugin_author = 'HelpForWP';
$_ct_menu_url = admin_url('options-general.php?page=campaigntracker');


if( !class_exists( 'EDD_SL_Plugin_Updater_4_CampaignTracker' ) ) {
	// load our custom updater
	require_once(dirname( __FILE__ ) . '/inc/EDD_SL_Plugin_Updater.php');
}

$_ct_license_key = trim( get_option( 'ct_license_key' ) );
// setup the updater
$_ct_updater = new EDD_SL_Plugin_Updater_4_CampaignTracker( $_ct_home_url, __FILE__, array( 
		'version' 	=> $_ct_version, 				// current version number
		'license' 	=> $_ct_license_key, 		// license key (used get_option above to retrieve from DB)
		'item_name' => $_ct_plugin_name, 	// name of this plugin
		'author' 	=> $_ct_plugin_author  // author of this plugin
	)
);

//for new version message and expiring version message shown on dashboard
if( !class_exists( 'EddSLUpdateExpiredMessagerV4forCampaignTracker' ) ) {
	// load our custom updater
	require_once(dirname( __FILE__ ) . '/inc/edd-sl-update-expired-messager.php');
}
$init_arg = array();
$init_arg['plugin_name'] = $_ct_plugin_name;
$init_arg['plugin_download_id'] = 20178;
$init_arg['plugin_folder'] = 'campaign-tracker';
$init_arg['plugin_file'] = basename(__FILE__);
$init_arg['plugin_version'] = $_ct_version;
$init_arg['plugin_home_url'] = $_ct_home_url;
$init_arg['plugin_sell_page_url'] = 'http://helpforwp.com/downloads/campaign-tracker/';
$init_arg['plugin_author'] = $_ct_plugin_author;
$init_arg['plugin_setting_page_url'] = $_ct_menu_url;
$init_arg['plugin_license_key_opiton_name'] = 'ct_license_key';
$init_arg['plugin_license_status_option_name'] = 'ct_license_key_status';
$_ct_messager = new EddSLUpdateExpiredMessagerV4forCampaignTracker( $init_arg );

class CampaignTracker{
	
	var $_ct_ajax_loader_image_url = '';
	var $_ct_trash_image_url = '';
	var $_ct_supported_module_instance = array();
	var $_ct_cookie_name = 'ct_tracking_cookie';
	var $_ct_gclid_cookie_name = 'ct_gclid_tracking_cookie';
	var $_ct_traffic_source_cookie_name = 'ct_traffic_source_cookie';
	
	var $_ct_plugin_settings_option = '_ct_plugin_settings_';
	var $_ct_license_key_option = 'ct_license_key';
	var $_ct_license_key_status_option = 'ct_license_key_status';
	
	var $_ct_campaign_settings_option = 'ct_campaign_settings';
	var $_ct_campaign_settings_posttype_to_hide_url_builder_option = 'ct_campaign_settings_posttype_to_hide_url_builder';
	var $_ct_campaign_settings_default_values = array();
	
	var $_ct_CLASS_option = NULL;
	var $_ct_CLASS_module_gravity_forms = NULL;
	var $_ct_CLASS_module_ninja_forms = NULL;
	var $_ct_CLASS_module_formidable_forms = NULL;
	var $_ct_CLASS_module_contact7_forms = NULL;
	
	public function __construct() {
		global $_ct_menu_url;
		
		$this->_ct_ajax_loader_image_url = plugin_dir_url("").'campaign-tracker/images/ajax-loader.gif';
		$this->_ct_trash_image_url = plugin_dir_url("").'campaign-tracker/images/trash.gif';
		$this->_ct_campaign_settings_default_values['source'] = array( 'twitter.com', 'facebook.com', 'linkedin.com', 'plus.google.com', 'youtube.com' );
		$this->_ct_campaign_settings_default_values['medium'] = array( 'social', 'email', 'feed', 'banner', 'cpc', 'display', 'affiliate', 'tv', 'radio' );
		
		if( is_admin() ) {
			add_action( 'admin_enqueue_scripts', array($this, 'ct_enqueue_scripts') );
			add_action( 'admin_init', array($this, 'ct_activate_license') );
			add_action( 'admin_init', array($this, 'ct_deactivate_license') );
		}
		
		//Plugin update actions
		register_uninstall_hook( __FILE__, 'CampaignTracker::ct_deinstall' );
		register_deactivation_hook( __FILE__, array($this, 'ct_deactivate') );
		
		//set cookie
		$plugin_settings = get_option( $this->_ct_plugin_settings_option, '' );
		if( !$plugin_settings || !is_array($plugin_settings) || count($plugin_settings) < 1 ||
			!isset($plugin_settings['populating_way']) || $plugin_settings['populating_way'] != 'javascript' ){
			
			add_action( 'init', array($this, 'ct_set_cookie_fun') );
		}else{
			add_action( 'wp_enqueue_scripts', array($this, 'ct_enqueue_scripts') );
			add_action( 'wp_ajax_ct_encode_cookie_data', array($this, 'ct_ajax_encode_cookie_data_fun') );
			add_action( 'wp_ajax_nopriv_ct_encode_cookie_data', array($this, 'ct_ajax_encode_cookie_data_fun') );
			add_action( 'wp_ajax_ct_decode_cookie_data', array($this, 'ct_ajax_ct_decode_cookie_data_fun') );
			add_action( 'wp_ajax_nopriv_ct_decode_cookie_data', array($this, 'ct_ajax_ct_decode_cookie_data_fun') );
			
			add_action( 'wp_footer', array($this, 'ct_set_cookie_js') );
		}
		
		add_action( 'init', array($this, 'ct_post_action') );
		
		//init modules
		require_once( 'inc/ct-module-gravity-forms.php' );
		require_once( 'inc/ct-module-ninja-forms.php' );
		require_once( 'inc/ct-module-formidable-forms.php' );
		require_once( 'inc/ct-module-contact7-forms.php' );
		$init_arg = array();
		$init_arg['license_key_option'] = $this->_ct_license_key_option;
		$init_arg['license_key_status_option'] = $this->_ct_license_key_status_option;
		$init_arg['ajax_loader_img_url'] = $this->_ct_ajax_loader_image_url;
		$init_arg['trash_img_url'] = $this->_ct_trash_image_url;
		$init_arg['cookie_name'] = $this->_ct_cookie_name;
		$init_arg['gclid_cookie_name'] = $this->_ct_gclid_cookie_name;
		$init_arg['traffic_cookie_name'] = $this->_ct_traffic_source_cookie_name;
		$init_arg['plugin_settings_option'] = $this->_ct_plugin_settings_option;
		
		$plugins = get_option( 'active_plugins' );
		//ninja forms
		if( in_array('ninja-forms/ninja-forms.php', $plugins) ) {
			$this->_ct_CLASS_module_ninja_forms = new CampaignTrackerModule_NinjaForms( $init_arg );
			$this->_ct_supported_module_instance['ninja_forms'] = $this->_ct_CLASS_module_ninja_forms;
		}else{
			//check again to see if gravityforms actived on siteside--multiple site
			$sitewide_plugins = get_site_option('active_sitewide_plugins');
			if( $sitewide_plugins && is_array($sitewide_plugins) && array_key_exists('ninja-forms/ninja-forms.php',$sitewide_plugins) ) {
				$this->_ct_CLASS_module_ninja_forms = new CampaignTrackerModule_NinjaForms( $init_arg );
				$this->_ct_supported_module_instance['ninja_forms'] = $this->_ct_CLASS_module_ninja_forms;
			}
		}
		//gravity forms
		if( in_array('gravityforms/gravityforms.php', $plugins) ) {
			$this->_ct_CLASS_module_gravity_forms = new CampaignTrackerModule_GravityForm( $init_arg );
			$this->_ct_supported_module_instance['gravity_forms'] = $this->_ct_CLASS_module_gravity_forms;
		}else{
			//check again to see if gravityforms actived on siteside--multiple site
			$sitewide_plugins = get_site_option('active_sitewide_plugins');
			if( $sitewide_plugins && is_array($sitewide_plugins) && array_key_exists('gravityforms/gravityforms.php',$sitewide_plugins) ) {
				$this->_ct_CLASS_module_gravity_forms = new CampaignTrackerModule_GravityForm( $init_arg );
				$this->_ct_supported_module_instance['gravity_forms'] = $this->_ct_CLASS_module_gravity_forms;
			}
		}
		//formidable forms
		if( in_array('formidable/formidable.php', $plugins) ) {
			$this->_ct_CLASS_module_formidable_forms = new CampaignTrackerModule_FormidableForms( $init_arg );
			$this->_ct_supported_module_instance['formidable_forms'] = $this->_ct_CLASS_module_formidable_forms;
		}else{
			//check again to see if gravityforms actived on siteside--multiple site
			$sitewide_plugins = get_site_option('active_sitewide_plugins');
			if( $sitewide_plugins && is_array($sitewide_plugins) && array_key_exists('formidable/formidable.php',$sitewide_plugins) ) {
				$this->_ct_CLASS_module_formidable_forms = new CampaignTrackerModule_FormidableForms( $init_arg );
				$this->_ct_supported_module_instance['formidable_forms'] = $this->_ct_CLASS_module_formidable_forms;
			}
		}
		//Contact7 forms
		if( in_array('contact-form-7/wp-contact-form-7.php', $plugins) ) {
			$this->_ct_CLASS_module_contact7_forms = new CampaignTrackerModule_Contact7Forms( $init_arg );
			$this->_ct_supported_module_instance['wpcf7_contact_form'] = $this->_ct_CLASS_module_contact7_forms;
		}else{
			//check again to see if gravityforms actived on siteside--multiple site
			$sitewide_plugins = get_site_option('active_sitewide_plugins');
			if( $sitewide_plugins && is_array($sitewide_plugins) && array_key_exists('contact-form-7/wp-contact-form-7.php',$sitewide_plugins) ) {
				$this->_ct_CLASS_module_contact7_forms = new CampaignTrackerModule_Contact7Forms( $init_arg );
				$this->_ct_supported_module_instance['wpcf7_contact_form'] = $this->_ct_CLASS_module_contact7_forms;
			}
		}
		
		//init options
		require_once( 'inc/ct-options.php' );
		
		$init_arg = array();
		$init_arg['plugin_page_url'] = $_ct_menu_url;
		$init_arg['license_key_option'] = 'ct_license_key';
		$init_arg['license_key_status_option'] = 'ct_license_key_status';
		$init_arg['ajax_loader_img_url'] = $this->_ct_ajax_loader_image_url;
		$init_arg['supported_modules'] = $this->_ct_supported_module_instance;
		$init_arg['plugin_settings_option'] = $this->_ct_plugin_settings_option;
		$init_arg['campaign_settings_option'] = $this->_ct_campaign_settings_option;
		$init_arg['posttypes_to_hide_url_builder'] = $this->_ct_campaign_settings_posttype_to_hide_url_builder_option;
		$init_arg['settings_default_values'] = $this->_ct_campaign_settings_default_values;
		
		$this->_ct_CLASS_option = new CampaignTrackerOption( $init_arg );
	}
	

	function ct_enqueue_scripts() {
		global $_ct_version;
		
		if( is_admin() ){
			wp_enqueue_script( 'ct-admin', plugin_dir_url( __FILE__ ) . 'js/ct-admin.js', array( 'jquery' ), $_ct_version );
			wp_enqueue_style( 'ct-admin', plugin_dir_url( __FILE__ ) . 'css/ct-style-admin.css', array(), $_ct_version );
		}else{
			wp_enqueue_script( 'jquery' );
			
			wp_enqueue_script( 'jquery-cookie', plugin_dir_url( __FILE__ ) . 'js/jquery.cookie.js', array( 'jquery' ), $_ct_version );
			wp_enqueue_script( 'ct-front', plugin_dir_url( __FILE__ ) . 'js/ct-front.js', array( 'jquery', 'jquery-cookie' ), $_ct_version );
		}
	}
	
	function ct_post_action(){
		if( isset( $_POST['ct_action'] ) && strlen($_POST['ct_action']) >0 ) {
			do_action( 'ct_action_' . $_POST['ct_action'], $_POST );
		}
	}
	
	function ct_deactivate() {
		if( function_exists('is_multisite') && is_multisite() ) {
			static $deact = 0;
			global $wpdb;
			// check if it is a network activation - if so, run the activation function for each blog id
			if (isset($_GET['networkwide']) && ($_GET['networkwide'] == 1)) {
				$old_blog = $wpdb->blogid;
				// Get all blog ids
				$blogids = $wpdb->get_col($wpdb->prepare("SELECT blog_id FROM $wpdb->blogs"));
				foreach ($blogids as $blog_id) {
					switch_to_blog($blog_id);
					delete_option('ct_license_key');
					delete_option('ct_license_key_status');
				}
				switch_to_blog($old_blog);
			} 
		}else{
			delete_option('ct_license_key');
			delete_option('ct_license_key_status');
		}
	}
	
	function ct_deinstall() {
		
		if( function_exists('is_multisite') && is_multisite() ) {
			static $deact = 0;
			global $wpdb;
			// check if it is a network activation - if so, run the activation function for each blog id
			if (isset($_GET['networkwide']) && ($_GET['networkwide'] == 1)) {
				$old_blog = $wpdb->blogid;
				// Get all blog ids
				$blogids = $wpdb->get_col($wpdb->prepare("SELECT blog_id FROM $wpdb->blogs"));
				foreach ($blogids as $blog_id) {
					switch_to_blog($blog_id);
					delete_option('ct_license_key');
					delete_option('ct_license_key_status');
					delete_option('_ct_plugin_settings_');
					
					delete_option('ct_campaign_settings');
					delete_option('ct_campaign_settings_max_id');
					delete_option('ct_campaign_settings_posttype_to_hide_url_builder');
				}
				switch_to_blog($old_blog);
			} 
		}else{
			delete_option('ct_license_key');
			delete_option('ct_license_key_status');
			delete_option('_ct_plugin_settings_');
			delete_option('ct_campaign_settings');
			delete_option('ct_campaign_settings_max_id');
			delete_option('ct_campaign_settings_posttype_to_hide_url_builder');
		}
	}
	
	function ct_activate_license() {
		// listen for our activate button to be clicked
		if( isset( $_POST['ct_license_activate'] ) ) {
			global $_ct_plugin_name, $_ct_home_url;
	
			// run a quick security check 
			if( ! check_admin_referer( 'ct_license_key_nonce', 'ct_license_key_nonce' ) ) 	
				return; // get out if we didn't click the Activate button
	
			// retrieve the license from the database
			$license = trim( $_POST['ct_license_key'] );
				
			// data to send in our API request
			$api_params = array( 
				'edd_action'=> 'activate_license', 
				'license' 	=> $license,
				'url'		=> get_option('home'),
				'item_name' => urlencode( $_ct_plugin_name ) // the name of our product in EDD
			);
			// Call the custom API.
			$response = wp_remote_get( add_query_arg( $api_params, $_ct_home_url ), array( 'timeout' => 15, 'sslverify' => false ) );
			// make sure the response came back okay
			if ( is_wp_error( $response ) )
				return false;
			// decode the license data
			$license_data = json_decode( wp_remote_retrieve_body( $response ) );
	
			update_option( 'ct_license_key', $license );
			if( $license_data && isset($license_data->license) ){
				update_option( 'ct_license_key_status', $license_data->license );
			}
		}
	}
	
	function ct_deactivate_license() {
		// listen for our activate button to be clicked
		if( isset( $_POST['ct_license_deactivate'] ) ) {
			global $_ct_plugin_name, $_ct_home_url;
			
			// run a quick security check 
			if( ! check_admin_referer( 'ct_license_key_nonce', 'ct_license_key_nonce' ) ) 	
				return; // get out if we didn't click the Activate button
	
			// retrieve the license from the database
			$license = trim( get_option( 'ct_license_key' ) );
				
	
			// data to send in our API request
			$api_params = array( 
				'edd_action'=> 'deactivate_license', 
				'license' 	=> $license, 
				'url'		=> get_option('home'),
				'item_name' => urlencode( $_ct_plugin_name ) // the name of our product in EDD
			);
			
			// Call the custom API.
			global $_ct_home_url;
			$response = wp_remote_get( add_query_arg( $api_params, $_ct_home_url ), array( 'timeout' => 15, 'sslverify' => false ) );
	
			// make sure the response came back okay
			if ( is_wp_error( $response ) )
				return false;
	
			// decode the license data
			$license_data = json_decode( wp_remote_retrieve_body( $response ) );
			
			// $license_data->license will be either "deactivated" or "failed"
			if( $license_data && isset($license_data->license) && $license_data->license == 'deactivated' )
				delete_option( 'ct_license_key_status' );
		}
	}
	
	function ct_set_cookie_fun(){
		$ct_license_key = get_option( $this->_ct_license_key_option );
		$ct_license_status = get_option( $this->_ct_license_key_status_option );
		if (!$ct_license_key || $ct_license_status != 'valid'){
			delete_option( $this->_ct_license_key_status_option );
			
			return false;
		}
		//read plugin settings
		$plugin_settings = get_option( $this->_ct_plugin_settings_option, '' );
		
		//for gclid
		$this->ct_set_gclid_cookie_fun( $plugin_settings, $this->_ct_gclid_cookie_name );
		//for traffic source
		$this->ct_set_traffic_source_cookie_fun( $plugin_settings, $this->_ct_traffic_source_cookie_name );
		
		//for campaign and custom varaibles
		$campaign_n_custom_values = $this->ct_get_campaign_n_custom_values( $plugin_settings );
		if( $campaign_n_custom_values == false ){
			return;
		}
		
		//check if cookie exist and not empty cookie then no need to do anything
		if( (isset($_COOKIE[$this->_ct_cookie_name]) && !$this->ct_check_if_empty_cookie_data($_COOKIE[$this->_ct_cookie_name])) ){
				
			return;
		}
		
		//save cookie
		$saved_cookie_days = 30;
		if( $plugin_settings && is_array($plugin_settings) && isset($plugin_settings['cookie_days']) ){
			$saved_cookie_days = $plugin_settings['cookie_days'];
		}
		$expire = time() + 3600*24*$saved_cookie_days;
		$secure = ( 'https' === parse_url( site_url(), PHP_URL_SCHEME ) && 'https' === parse_url( home_url(), PHP_URL_SCHEME ) );
		setcookie( $this->_ct_cookie_name, $campaign_n_custom_values, $expire, COOKIEPATH, COOKIE_DOMAIN, $secure );
		if ( SITECOOKIEPATH != COOKIEPATH ){
			setcookie( $this->_ct_cookie_name, $campaign_n_custom_values, $expire, SITECOOKIEPATH, COOKIE_DOMAIN, $secure );
		}
		
		$_COOKIE[$this->_ct_cookie_name] = $campaign_n_custom_values;
	}
	
	function ct_get_campaign_n_custom_values( $plugin_settings ){
		
		$ct_cookie_data = array();
		
		//google variables
		if( isset( $_REQUEST['utm_source'] ) && 
			isset( $_REQUEST['utm_medium'] ) && 
			isset( $_REQUEST['utm_campaign'] ) ){
			
			$utm_source = $_REQUEST['utm_source'];
			$utm_medium = $_REQUEST['utm_medium'];
			$utm_campaign = $_REQUEST['utm_campaign'];
			$utm_term = isset( $_REQUEST['utm_term'] ) ? $_REQUEST['utm_term'] : '';
			$utm_content = isset( $_REQUEST['utm_content'] ) ? $_REQUEST['utm_content'] : '';
			
			$ct_cookie_data['s'] = $utm_source;
			$ct_cookie_data['m'] = $utm_medium;
			$ct_cookie_data['t'] = $utm_term;
			$ct_cookie_data['c'] = $utm_content;
			$ct_cookie_data['ca'] = $utm_campaign;
		}
		
		//custom variables
		$custom_variables_settings = array();
		if( $plugin_settings && is_array($plugin_settings) && isset($plugin_settings['custom_variables']) ){
			$custom_variables_settings = $plugin_settings['custom_variables'];
		}
		
		if( isset($custom_variables_settings['var_1']) && $custom_variables_settings['var_1'] ){
			$variables_name = $custom_variables_settings['var_1'];
			if( isset( $_REQUEST[$variables_name] ) ){
				$ct_cookie_data['var_1'] = $_REQUEST[$variables_name];
			}
		}
		if( isset($custom_variables_settings['var_2']) && $custom_variables_settings['var_2'] ){
			$variables_name = $custom_variables_settings['var_2'];
			if( isset( $_REQUEST[$variables_name] ) ){
				$ct_cookie_data['var_2'] = $_REQUEST[$variables_name];
			}
		}
		if( isset($custom_variables_settings['var_3']) && $custom_variables_settings['var_3'] ){
			$variables_name = $custom_variables_settings['var_3'];
			if( isset( $_REQUEST[$variables_name] ) ){
				$ct_cookie_data['var_3'] = $_REQUEST[$variables_name];
			}
		}
		if( isset($custom_variables_settings['var_4']) && $custom_variables_settings['var_4'] ){
			$variables_name = $custom_variables_settings['var_4'];
			if( isset( $_REQUEST[$variables_name] ) ){
				$ct_cookie_data['var_4'] = $_REQUEST[$variables_name];
			}
		}
		if( isset($custom_variables_settings['var_5']) && $custom_variables_settings['var_5'] ){
			$variables_name = $custom_variables_settings['var_5'];
			if( isset( $_REQUEST[$variables_name] ) ){
				$ct_cookie_data['var_5'] = $_REQUEST[$variables_name];
			}
		}
		if( isset($custom_variables_settings['var_6']) && $custom_variables_settings['var_6'] ){
			$variables_name = $custom_variables_settings['var_6'];
			if( isset( $_REQUEST[$variables_name] ) ){
				$ct_cookie_data['var_6'] = $_REQUEST[$variables_name];
			}
		}
		
		if( count($ct_cookie_data) < 1 ){
			return false;
		}
		
		return base64_encode(serialize($ct_cookie_data));
	}
	
	function ct_set_gclid_cookie_fun( $plug_settings_data, $cookie_name ){
		
		$gclid_cookie_data = $this->ct_get_gclid_value();
		if( $gclid_cookie_data == false ){
			return;
		}
		
		//save cookie
		$saved_cookie_days = 30;
		if( $plug_settings_data && is_array($plug_settings_data) && isset($plug_settings_data['cookie_days']) ){
			$saved_cookie_days = $plug_settings_data['cookie_days'];
		}
		$expire = time() + 3600*24*$saved_cookie_days;
		$secure = ( 'https' === parse_url( site_url(), PHP_URL_SCHEME ) && 'https' === parse_url( home_url(), PHP_URL_SCHEME ) );
		setcookie( $cookie_name, $gclid_cookie_data, $expire, COOKIEPATH, COOKIE_DOMAIN, $secure );
		if ( SITECOOKIEPATH != COOKIEPATH ){
			setcookie( $cookie_name, $gclid_cookie_data, $expire, SITECOOKIEPATH, COOKIE_DOMAIN, $secure );
		}

		$_COOKIE[$cookie_name] = $gclid_cookie_data;
	}
	
	function ct_get_gclid_value(){
		//google gclid
		if( !isset( $_REQUEST['gclid'] ) || !$_REQUEST['gclid'] ){
			return false;
		}
		$ct_cookie_data = array();
		$ct_cookie_data['gclid'] = $_REQUEST['gclid'];
		
		return base64_encode(serialize($ct_cookie_data));
	}
	
	function ct_set_traffic_source_cookie_fun( $plug_settings_data, $cookie_name ){
		//check if cookie exist and not empty cookie then no need to do anything
		if( (isset($_COOKIE[$cookie_name]) && $_COOKIE[$cookie_name]) ){
			return;
		}

		$traffic_source_cookie_data = $this->ct_get_traffic_source_value( false );
		
		//save cookie
		$saved_cookie_days = 30;
		if( $plug_settings_data && is_array($plug_settings_data) && isset($plug_settings_data['cookie_days']) ){
			$saved_cookie_days = $plug_settings_data['cookie_days'];
		}
		$expire = time() + 3600*24*$saved_cookie_days;
		$secure = ( 'https' === parse_url( site_url(), PHP_URL_SCHEME ) && 'https' === parse_url( home_url(), PHP_URL_SCHEME ) );
		setcookie( $cookie_name, $traffic_source_cookie_data, $expire, COOKIEPATH, COOKIE_DOMAIN, $secure );
		if ( SITECOOKIEPATH != COOKIEPATH ){
			setcookie( $cookie_name, $traffic_source_cookie_data, $expire, SITECOOKIEPATH, COOKIE_DOMAIN, $secure );
		}

		$_COOKIE[$cookie_name] = $traffic_source_cookie_data;
	}
	
	function ct_get_traffic_source_value( $is_ajax ){
		$ct_cookie_data = array();
		$traffic_source = '';
		if( $is_ajax ){
			if( isset($_REQUEST['referrer']) ){
				$traffic_source =  $_REQUEST['referrer'];
			}else{
				return false;
			}
		}else{
			if( isset($_SERVER['HTTP_REFERER']) ){
				$traffic_source = wp_unslash( $_SERVER['HTTP_REFERER'] );
			}
		}
		if( !$traffic_source ){
			$traffic_source = 'Direct';
		}
		$ct_cookie_data['traffic_source'] = $traffic_source;
		
		return base64_encode(serialize($ct_cookie_data));
	}
	
	function ct_check_if_empty_cookie_data( $cookie_data ){
		if( !is_array($cookie_data) ){
			$gclid_cookies_data = base64_decode($cookie_data);
			$gclid_cookies_data_array = unserialize($gclid_cookies_data);
		}else{
			$gclid_cookies_data = $cookie_data;
		}
		if( !$gclid_cookies_data_array || !is_array($gclid_cookies_data_array) || count($gclid_cookies_data_array) < 1 ){
			return true;
		}
		foreach( $gclid_cookies_data_array as $key => $node ){
			if( $node ){
				return false;
			}
		}
		
		return true;
	}
	
	function ct_set_cookie_js() {
		//read plugin settings
		$plugin_settings = get_option( $this->_ct_plugin_settings_option, '' );
		
		//cookie parameters for ajax
		$saved_cookie_days = 30;
		if( $plugin_settings && is_array($plugin_settings) && isset($plugin_settings['cookie_days']) ){
			$saved_cookie_days = $plugin_settings['cookie_days'];
		}

		$secure = ( 'https' === parse_url( site_url(), PHP_URL_SCHEME ) && 'https' === parse_url( home_url(), PHP_URL_SCHEME ) );
		$path = COOKIEPATH;
		if ( SITECOOKIEPATH != COOKIEPATH ){
			$path = SITECOOKIEPATH;
		}
		$secure_string = $secure ? 'true' : 'false';
	?>
		<script type="text/javascript" >
        jQuery(document).ready(function($) {
			var var_gclid = '';
    		var var_referrer = '';
			var var_utm_source = '';
			var var_utm_medium = '';
			var var_utm_term = '';
			var var_utm_content = '';
			var var_utm_campaign = '';
			<?php
			//custom variables
			$custom_variables_settings = array();
			$custom_valid_variables = array();
			if( $plugin_settings && is_array($plugin_settings) && isset($plugin_settings['custom_variables']) ){
				$custom_variables_settings = $plugin_settings['custom_variables'];
			}
			if( is_array($custom_variables_settings) && count($custom_variables_settings) > 0 ){
				foreach( $custom_variables_settings as $var_key => $var_name ){
					echo 'var '.$var_key.' = \'\';'."\n";
					if( !$var_name ){
						continue;
					}
					$custom_valid_variables[$var_key] = $var_name;
				}
			}else{
				echo 'var var_1 = \'\';'."\n";
				echo 'var var_2 = \'\';'."\n";
				echo 'var var_3 = \'\';'."\n";
				echo 'var var_4 = \'\';'."\n";
				echo 'var var_5 = \'\';'."\n";
				echo 'var var_6 = \'\';'."\n";
			}
			?>
			
			var cookie_parameters = { expires : <?php echo $saved_cookie_days ?>, path : '<?php echo $path; ?>', domain: '<?php echo COOKIE_DOMAIN; ?>', secure: <?php echo $secure_string; ?> };

			/////////read cookie
			var var_gclid_to_decode = '';
			var var_referrer_to_decoded = '';
			var var_campaing_n_customs_to_decoded = '';
			
			//gclid
			var exist_gclid_cookie_data = jQuery.cookie('<?php echo $this->_ct_gclid_cookie_name; ?>');
			if( exist_gclid_cookie_data ){
				var_gclid_to_decode = exist_gclid_cookie_data;
			}else{
				var_gclid = get_url_parameter_by_name( 'gclid' );
			}
			//traffic
			var exist_traffic_source_cookie_data = jQuery.cookie('<?php echo $this->_ct_traffic_source_cookie_name; ?>');
			if( exist_traffic_source_cookie_data ){
				var_referrer_to_decoded = exist_traffic_source_cookie_data;
			}else{
				var_referrer = document.referrer;
				if( var_referrer == "" ){
					var_referrer = 'Direct';
				}
			}
			//campaign and custom variables
			var exist_campaign_n_customs_cookie_data = jQuery.cookie('<?php echo $this->_ct_cookie_name; ?>');
			if( exist_campaign_n_customs_cookie_data ){
				var_campaing_n_customs_to_decoded = exist_campaign_n_customs_cookie_data;
			}else{
				var_utm_source = get_url_parameter_by_name( 'utm_source' );
				var_utm_medium = get_url_parameter_by_name( 'utm_medium' );
				var_utm_term = get_url_parameter_by_name( 'utm_term' );
				var_utm_content = get_url_parameter_by_name( 'utm_content' );
				var_utm_campaign = get_url_parameter_by_name( 'utm_campaign' );
				<?php
				if( count($custom_valid_variables) > 0 ){
					foreach( $custom_valid_variables as $var_key => $var_name ){
						echo $var_key.' = get_url_parameter_by_name(\''.$var_name.'\');'."\n";
					}
				}
				?>
			}
			//use ajax to decode data
			if( var_gclid_to_decode || var_referrer_to_decoded || var_campaing_n_customs_to_decoded ){
				var data = {
					'action': 'ct_decode_cookie_data',
					'gclid_data': var_gclid_to_decode,
					'traffic_data': var_referrer_to_decoded,
					'camaign_n_customs_data': var_campaing_n_customs_to_decoded
				}
				jQuery.post('<?php echo admin_url('admin-ajax.php'); ?>', data, function(response) {
					if( response.indexOf("ERROR") != -1 ){
						alert( response );
						return;
					}
					var return_obj = jQuery.parseJSON( response );
					//gclid
					if( var_gclid_to_decode ){
						var_gclid = return_obj.gclid;
					}
					//traffic
					if( var_referrer_to_decoded ){
						var_referrer = return_obj.referrer;
					}
					//campaing & customs
					if( var_campaing_n_customs_to_decoded ){
						var_utm_source = return_obj.s;
						var_utm_medium =  return_obj.m;
						var_utm_term =  return_obj.t;
						var_utm_content =  return_obj.c;
						var_utm_campaign =  return_obj.ca;
						<?php
						if( count($custom_valid_variables) > 0 ){
							foreach( $custom_valid_variables as $var_key => $var_name ){
								echo $var_key.' = return_obj.'.$var_key.';'."\n";
							}
						}
						?>
					}
					
					/////////set cookie
					var data_2_encode = {
										'gclid': '',
										'referrer': '',
										'utm_source': '',
										'utm_medium': '',
										'utm_term': '',
										'utm_content': '',
										'utm_campaign': '',
										<?php
										if( count($custom_valid_variables) > 0 ){
											foreach( $custom_valid_variables as $var_key => $var_name ){
												echo "'".$var_name."': ".$var_key.",\n";
											}
										}
										?>
									};
					//gclid
					var_gclid = get_url_parameter_by_name( 'gclid' );
					if( var_gclid == "" ){
						delete data_2_encode['gclid'];
						//if there's new glicd then just use the saved cookie if there it is
						if( var_gclid_to_decode ){
							var_gclid = return_obj.gclid;
						}
					}else{
						data_2_encode.gclid = var_gclid;
					}
					//trafic source
					if( var_referrer_to_decoded == "" ){
						if( var_referrer == "" ){
							delete data_2_encode['referrer']; 
						}else{
							data_2_encode.referrer = var_referrer;
						}
					}else{
						delete data_2_encode['referrer']; 
					}
					
					if( var_campaing_n_customs_to_decoded == "" ){
						if( var_utm_source == "" ){
							delete data_2_encode['utm_source']; 
						}else{
							data_2_encode.utm_source = var_utm_source;
						}
						if( var_utm_medium == "" ){
							delete data_2_encode['utm_medium']; 
						}else{
							data_2_encode.utm_medium = var_utm_medium;
						}
						if( var_utm_term == "" ){
							delete data_2_encode['utm_term']; 
						}else{
							data_2_encode.utm_term = var_utm_term;
						}
						if( var_utm_content == "" ){
							delete data_2_encode['utm_content']; 
						}else{
							data_2_encode.utm_content = var_utm_content;
						}
						if( var_utm_campaign == "" ){
							delete data_2_encode['utm_campaign']; 
						}else{
							data_2_encode.utm_campaign = var_utm_campaign;
						}
						<?php
						if( count($custom_valid_variables) > 0 ){
							foreach( $custom_valid_variables as $var_key => $var_name ){
								echo   'if( '.$var_key.' == "" ){
											delete data_2_encode[\''.$var_name.'\']; 
										}else{
											data_2_encode.var_name = '.$var_key.';
										}';
							}
						}
						?>
					}else{
						delete data_2_encode['utm_source'];
						delete data_2_encode['utm_medium'];
						delete data_2_encode['utm_term'];
						delete data_2_encode['utm_content'];
						delete data_2_encode['utm_campaign'];
						<?php
						if( count($custom_valid_variables) > 0 ){
							foreach( $custom_valid_variables as $var_key => $var_name ){
								echo   'delete data_2_encode[\''.$var_name.'\'];';
							}
						}
						?>
					}
					
					if( !jQuery.isEmptyObject( data_2_encode ) ){
						data_2_encode['action'] = 'ct_encode_cookie_data';
						jQuery.post('<?php echo admin_url('admin-ajax.php'); ?>', data_2_encode, function(response) {
							if( response.indexOf("ERROR") != -1 ){
								alert( response );
								return;
							}
							var return_obj = jQuery.parseJSON( response );
							//gclid
							if( return_obj.gclid ){
								jQuery.cookie( '<?php echo $this->_ct_gclid_cookie_name; ?>', return_obj.gclid, cookie_parameters );
							}
							//traffic
							if( return_obj.referrer ){
								jQuery.cookie( '<?php echo $this->_ct_traffic_source_cookie_name; ?>', return_obj.referrer, cookie_parameters );
							}
							//campaing & customs
							if( return_obj.campaign_n_customs ){
								jQuery.cookie( '<?php echo $this->_ct_cookie_name; ?>', return_obj.campaign_n_customs, cookie_parameters );
							}
							
							//populate forms
							<?php
							//populate gravity forms
							if( $this->_ct_CLASS_module_gravity_forms ){
								$this->_ct_CLASS_module_gravity_forms->ct_module_js_populate_forms();
							}
							
							//populate ninja forms
							if( $this->_ct_CLASS_module_ninja_forms ){
								$this->_ct_CLASS_module_ninja_forms->ct_module_js_populate_forms();
							}
							//populate formidable forms
							if( $this->_ct_CLASS_module_formidable_forms ){
								$this->_ct_CLASS_module_formidable_forms->ct_module_js_populate_forms();
							}
							//populate contact form 7 forms
							if( $this->_ct_CLASS_module_contact7_forms ){
								$this->_ct_CLASS_module_contact7_forms->ct_module_js_populate_forms();
							}
							?>
						});
					}else{
						//populate forms
						<?php
						//populate gravity forms
						if( $this->_ct_CLASS_module_gravity_forms ){
							$this->_ct_CLASS_module_gravity_forms->ct_module_js_populate_forms();
						}
						
						//populate ninja forms
						if( $this->_ct_CLASS_module_ninja_forms ){
							$this->_ct_CLASS_module_ninja_forms->ct_module_js_populate_forms();
						}
						//populate formidable forms
						if( $this->_ct_CLASS_module_formidable_forms ){
							$this->_ct_CLASS_module_formidable_forms->ct_module_js_populate_forms();
						}
						//populate contact form 7 forms
						if( $this->_ct_CLASS_module_contact7_forms ){
							$this->_ct_CLASS_module_contact7_forms->ct_module_js_populate_forms();
						}
						?>
					}
				});
			}else{
				/////////set cookie
				var data_2_encode = {
									'gclid': '',
									'referrer': '',
									'utm_source': '',
									'utm_medium': '',
									'utm_term': '',
									'utm_content': '',
									'utm_campaign': '',
									<?php
									if( count($custom_valid_variables) > 0 ){
										foreach( $custom_valid_variables as $var_key => $var_name ){
											echo "'".$var_name."': ".$var_key.",\n";
										}
									}
									?>
								};
				//gclid
				var_gclid = get_url_parameter_by_name( 'gclid' );
				if( var_gclid == "" ){
					delete data_2_encode['gclid'];
					//if there's new glicd then just use the saved cookie if there it is
					if( var_gclid_to_decode ){
						var_gclid = return_obj.gclid;
					}
				}else{
					data_2_encode.gclid = var_gclid;
				}
				//trafic source
				if( var_referrer_to_decoded == "" ){
					if( var_referrer == "" ){
						delete data_2_encode['referrer']; 
					}else{
						data_2_encode.referrer = var_referrer;
					}
				}else{
					delete data_2_encode['referrer']; 
				}
				
				if( var_campaing_n_customs_to_decoded == "" ){
					if( var_utm_source == "" ){
						delete data_2_encode['utm_source']; 
					}else{
						data_2_encode.utm_source = var_utm_source;
					}
					if( var_utm_medium == "" ){
						delete data_2_encode['utm_medium']; 
					}else{
						data_2_encode.utm_medium = var_utm_medium;
					}
					if( var_utm_term == "" ){
						delete data_2_encode['utm_term']; 
					}else{
						data_2_encode.utm_term = var_utm_term;
					}
					if( var_utm_content == "" ){
						delete data_2_encode['utm_content']; 
					}else{
						data_2_encode.utm_content = var_utm_content;
					}
					if( var_utm_campaign == "" ){
						delete data_2_encode['utm_campaign']; 
					}else{
						data_2_encode.utm_campaign = var_utm_campaign;
					}
					<?php
					if( count($custom_valid_variables) > 0 ){
						foreach( $custom_valid_variables as $var_key => $var_name ){
							echo   'if( '.$var_key.' == "" ){
										delete data_2_encode[\''.$var_name.'\']; 
									}else{
										data_2_encode.var_name = '.$var_key.';
									}';
						}
					}
					?>
				}else{
					delete data_2_encode['utm_source'];
					delete data_2_encode['utm_medium'];
					delete data_2_encode['utm_term'];
					delete data_2_encode['utm_content'];
					delete data_2_encode['utm_campaign'];
					<?php
					if( count($custom_valid_variables) > 0 ){
						foreach( $custom_valid_variables as $var_key => $var_name ){
							echo   'delete data_2_encode[\''.$var_name.'\'];';
						}
					}
					?>
				}
				
				if( !jQuery.isEmptyObject( data_2_encode ) ){
					data_2_encode['action'] = 'ct_encode_cookie_data';
					jQuery.post('<?php echo admin_url('admin-ajax.php'); ?>', data_2_encode, function(response) {
						if( response.indexOf("ERROR") != -1 ){
							alert( response );
							return;
						}
						var return_obj = jQuery.parseJSON( response );
						//gclid
						if( return_obj.gclid ){
							jQuery.cookie( '<?php echo $this->_ct_gclid_cookie_name; ?>', return_obj.gclid, cookie_parameters );
						}
						//traffic
						if( return_obj.referrer ){
							jQuery.cookie( '<?php echo $this->_ct_traffic_source_cookie_name; ?>', return_obj.referrer, cookie_parameters );
						}
						//campaing & customs
						if( return_obj.campaign_n_customs ){
							jQuery.cookie( '<?php echo $this->_ct_cookie_name; ?>', return_obj.campaign_n_customs, cookie_parameters );
						}
						
						//populate forms
						<?php
						//populate gravity forms
						if( $this->_ct_CLASS_module_gravity_forms ){
							$this->_ct_CLASS_module_gravity_forms->ct_module_js_populate_forms();
						}
						
						//populate ninja forms
						if( $this->_ct_CLASS_module_ninja_forms ){
							$this->_ct_CLASS_module_ninja_forms->ct_module_js_populate_forms();
						}
						//populate formidable forms
						if( $this->_ct_CLASS_module_formidable_forms ){
							$this->_ct_CLASS_module_formidable_forms->ct_module_js_populate_forms();
						}
						//populate contact form 7 forms
						if( $this->_ct_CLASS_module_contact7_forms ){
							$this->_ct_CLASS_module_contact7_forms->ct_module_js_populate_forms();
						}
						?>
					});
				}else{
					//populate forms
					<?php
					//populate gravity forms
					if( $this->_ct_CLASS_module_gravity_forms ){
						$this->_ct_CLASS_module_gravity_forms->ct_module_js_populate_forms();
					}
					
					//populate ninja forms
					if( $this->_ct_CLASS_module_ninja_forms ){
						$this->_ct_CLASS_module_ninja_forms->ct_module_js_populate_forms();
					}
					//populate formidable forms
					if( $this->_ct_CLASS_module_formidable_forms ){
						$this->_ct_CLASS_module_formidable_forms->ct_module_js_populate_forms();
					}
					//populate contact form 7 forms
					if( $this->_ct_CLASS_module_contact7_forms ){
						$this->_ct_CLASS_module_contact7_forms->ct_module_js_populate_forms();
					}
					?>
				}
			}
			
        });
        </script> 
	<?php
    }
	
	function ct_ajax_ct_decode_cookie_data_fun(){
		$ct_license_key = get_option( $this->_ct_license_key_option );
		$ct_license_status = get_option( $this->_ct_license_key_status_option );
		if (!$ct_license_key || $ct_license_status != 'valid'){
			delete_option( $this->_ct_license_key_status_option );
			
			wp_die('ERROR-Invalid license');
		}

		$return_array = array();
		if( isset($_REQUEST['gclid_data']) && $_REQUEST['gclid_data'] ){
			$gclid_data_decoded = unserialize( base64_decode($_REQUEST['gclid_data']) );
			$return_array['gclid'] = $gclid_data_decoded['gclid'];
		}
		if( isset($_REQUEST['traffic_data']) && $_REQUEST['traffic_data'] ){
			$traffice_source_decoded = unserialize( base64_decode($_REQUEST['traffic_data']) );
			$return_array['referrer'] = $traffice_source_decoded['traffic_source'];
		}
		if( isset($_REQUEST['camaign_n_customs_data']) ){
			$camaign_n_customs_data_decoded = unserialize( base64_decode($_REQUEST['camaign_n_customs_data']) );
			if( is_array($camaign_n_customs_data_decoded) && count($camaign_n_customs_data_decoded) > 0 ){
				$return_array = array_merge($return_array, $camaign_n_customs_data_decoded);
			}
		}
		
		wp_die( json_encode( $return_array ) );
	}
	
	function ct_ajax_encode_cookie_data_fun(){
		$ct_license_key = get_option( $this->_ct_license_key_option );
		$ct_license_status = get_option( $this->_ct_license_key_status_option );
		if (!$ct_license_key || $ct_license_status != 'valid'){
			delete_option( $this->_ct_license_key_status_option );
			
			wp_die('ERROR-Invalid license');
		}
		
		$return_obj = array( 'gclid' => '', 'referrer' => '', 'campaign_n_customs' => '' );
		//read plugin settings
		$plugin_settings = get_option( $this->_ct_plugin_settings_option, '' );
		
		//for gclid
		$gclid_cookie_data = $this->ct_get_gclid_value();
		if( $gclid_cookie_data != false ){
			$return_obj['gclid'] = $gclid_cookie_data;
		}
		
		//for gclid
		$traffic_source = $this->ct_get_traffic_source_value( true );
		if( $traffic_source != false ){
			$return_obj['referrer'] = $traffic_source;
		}
		
		//for campaign and custom varaibles
		$campaign_n_custom_values = $this->ct_get_campaign_n_custom_values( $plugin_settings );
		if( $campaign_n_custom_values != false ){
			$return_obj['campaign_n_customs'] = $campaign_n_custom_values;
		}
		
		wp_die( json_encode($return_obj) );
	}
}

$ct_pro_instance = new CampaignTracker();

