<?php

class CampaignTrackerOption{
	var $_ct_license_key_option = '';
	var $_ct_license_key_status_option = '';
	var $_ct_ajax_loader_image_url = '';
	
	var $_ct_plugin_settings_option = '';
	var $_ct_supported_modules = array();
	
	var $_ct_options_campaign_settings_OBJECT = NULL;
	
	public function __construct( $args ) {
		$this->_ct_ajax_loader_image_url = $args['ajax_loader_img_url'];
		$this->_ct_license_key_option = $args['license_key_option'];
		$this->_ct_license_key_status_option = $args['license_key_status_option'];
		$this->_ct_supported_modules = $args['supported_modules'];
		$this->_ct_plugin_settings_option = $args['plugin_settings_option'];
		
		add_action( 'admin_menu', array($this, 'ct_options_menu') );
		add_action( 'ct_action_save_custom_variable_settings', array($this, 'ct_save_custom_variables_settings_fun') );
		
		add_action( 'wp_ajax_ct_save_cookie_days', array($this, 'ct_save_cookie_days_fun') );
		add_action( 'wp_ajax_ct_save_populating_way', array($this, 'ct_save_populating_way_fun') );
		
		require_once( 'ct-options-campaign-settings.php' );
		
		$this->_ct_options_campaign_settings_OBJECT = new CampaignTrackerOptionCampaignSettings( $args );
	}
	
	function ct_options_menu() {
		
		add_options_page( 'Campaign Tracker', 
						  'Campaign Tracker', 
						  'manage_options', 
						  'campaigntracker', 
						  array($this, 'ct_options')
						);
	}
	
	function ct_options() {
		if (!current_user_can('manage_options'))  {
			wp_die( __('You do not have sufficient permissions to access this page.') );
		}
		$ct_license_key = get_option( $this->_ct_license_key_option );
		$ct_license_status = get_option( $this->_ct_license_key_status_option );
		if (!$ct_license_key || $ct_license_status != 'valid'){
			$ct_license_status = 'invalid';
			
			delete_option( $this->_ct_license_key_status_option );
		}
		
		$action = $_SERVER["REQUEST_URI"];
		?>
		<div class="wrap" id="ct_options_form_ID">
            <img src="<?PHP echo plugins_url(); ?>/campaign-tracker/images/help-for-wordpress-small.png" align="left"/>
            <h2>Campaign Tracker for WordPress</h2>
            <h2 class="nav-tab-wrapper">
                <a class="nav-tab nav-tab-active" href="javascript:void(0);" id="general-settings">General settings</a>
                <?php 
				if ( $ct_license_status !== false && $ct_license_status == 'valid' ) { ?>
                    <a class="nav-tab" href="javascript:void(0);" id="custom-variables">Custom variables</a>
                    <a class="nav-tab" href="javascript:void(0);" id="campaign-settings">Campaign Settings</a>
                    <?php
                    if( is_array($this->_ct_supported_modules) && count($this->_ct_supported_modules) > 0 && 
                        isset($this->_ct_supported_modules['gravity_forms']) ){
                    ?>
                    <a class="nav-tab" href="javascript:void(0);" id="gravity-forms">Gravity Forms</a>
                    <?php
                    }
                    if( is_array($this->_ct_supported_modules) && count($this->_ct_supported_modules) > 0 && 
                        isset($this->_ct_supported_modules['ninja_forms']) ){
                    ?>
                    <a class="nav-tab" href="javascript:void(0);" id="ninja-forms">Ninja Forms</a>
                <?php
					}
					if( is_array($this->_ct_supported_modules) && count($this->_ct_supported_modules) > 0 && 
                        isset($this->_ct_supported_modules['formidable_forms']) ){
                    ?>
                    <a class="nav-tab" href="javascript:void(0);" id="ninja-forms">Formidable Forms</a>
                <?php
					}
					if( is_array($this->_ct_supported_modules) && count($this->_ct_supported_modules) > 0 && 
                        isset($this->_ct_supported_modules['wpcf7_contact_form']) ){
                    ?>
                    <a class="nav-tab" href="javascript:void(0);" id="contact7-forms">Contact Form 7</a>
                <?php
					}
				}//end of license check
				?>
            </h2>
        
            <div id="ct_tab_contents">
                <section>
                	<form action="<?php echo $action; ?>" method="POST" id="ct_general_settings_form_id">
                	<h3>Please activate your plugin!</h3>
                    <p>In the field below please enter your license key to activate this plugin</p>
                    <p>
                    <input id="ct_license_key_id" name="ct_license_key" type="text" value="<?php echo $ct_license_key; ?>" size="50" />
                    <?php
                    if( $ct_license_status !== false && $ct_license_status == 'valid' ) {
                        echo '<span style="color:green;">Active</span>';
                        echo '<input type="submit" class="button-secondary" name="ct_license_deactivate" value="Deactivate License" style="margin-left:20px;" />';
                    }else{
                        if ($ct_license_key !== false && strlen($ct_license_key) > 0) { 
                            echo '<span style="color:red;">Inactive</span>'; 
                        }
                        echo '<input type="submit" class="button-secondary" name="ct_license_activate" value="Activate License" style="margin-left:20px;" />';
                    }
                    wp_nonce_field( 'ct_license_key_nonce', 'ct_license_key_nonce' ); 
                    ?>
                    </p>
                    <br />
                    <h3>Getting Started</h3>
                    <p>Campaign Tracker supports Google's campaign URLs, <a href="https://helpforwp.com/?p=21598&utm_source=PluginSettings&utm_medium=Campaign%20Tracker&utm_campaign=Campaign%20Tracker" target="_blank">learn more about these in this tutorial</a>. You can now create these tracking URL right inside your WordPress editor, <a href="https://helpforwp.com/plugins/campaign-tracker-documentation/?utm_source=PluginSettings&utm_medium=Campaign%20Tracker&utm_campaign=Campaign%20Tracker" target="_blank">visit the plugin documentation page to learn more.</a></p>
                    <p>Campaign Tracker for WordPress also supports creating your own custom URL variables to pass more values through to your forms. Create up to 6 of these by clicking on the "Custom Variables" tab.</p>
                    <p>Then you can map the URL variables to hidden fields in your forms. We support Gravity Forms, Ninja Forms and Formidable. Depending on which of these you have activated you will see a tab in which you can manage your mappings.</p>
					<p>We also support collecting the "GCLID" value that is used by Google Adwords. You will see when mapping a form you can select a hidden field that will pass through this special ID from Google. This will be useful if you have a process in place to process these IDs to match up with your leads (for example in Zoho CRM)</p>
					<?php
                    if( $ct_license_status !== false && $ct_license_status == 'valid' ) {
                    ?>
                    <br />
                    <h3>Cookie</h3>
                    <p>
                    <label>How many days should a cookie remain set by this plugin:</label>
                    <select id="ct_settings_cookie_days_ID" style="margin-left:20px;">
                    	<?php
						$cookie_days_array = array(1, 3, 5, 7, 30, 60, 90, 120);
						$saved_cookie_days = 30;
						$plugin_settings = get_option( $this->_ct_plugin_settings_option, '' );
						if( $plugin_settings && is_array($plugin_settings) && isset($plugin_settings['cookie_days']) ){
							$saved_cookie_days = $plugin_settings['cookie_days'];
						}
						foreach( $cookie_days_array as $cookie_day ){
							if( $saved_cookie_days == $cookie_day ){
								echo '<option value="'.$cookie_day.'" selected="selected">'.$cookie_day.'</option>';
							}else{
								echo '<option value="'.$cookie_day.'">'.$cookie_day.'</option>';
							}
						}
						?>
                    </select>
                    <span id="ct_settings_cookie_days_ajax_loader_ID" style="display: none;">
                        <img src="<?php echo $this->_ct_ajax_loader_image_url; ?>" />
                    </span>
                    </p>
                    <p>
                    <label>How would you like to populate your forms:</label>
                    <select id="ct_settings_cookie_populating_way_ID" style="margin-left:20px;">
                    	<?php
						$saved_populating_way = 'php';
						$plugin_settings = get_option( $this->_ct_plugin_settings_option, '' );
						if( $plugin_settings && is_array($plugin_settings) && isset($plugin_settings['populating_way']) ){
							$saved_populating_way = $plugin_settings['populating_way'];
						}
						if( $saved_populating_way == 'php' ){
							echo '<option value="php" selected="selected">WordPress Hook</option>';
						}else{
							echo '<option value="php">WordPress Hook</option>';
						}
						if( $saved_populating_way == 'javascript' ){
							echo '<option value="javascript" selected="selected">JavaScript</option>';
						}else{
							echo '<option value="javascript">JavaScript</option>';
						}
						?>
                    </select>
                    <span id="ct_settings_cookie_populating_way_ajax_loader_ID" style="display: none;">
                        <img src="<?php echo $this->_ct_ajax_loader_image_url; ?>" />
                    </span>
                    <br />
                    <i>If you use JavaScript then please clear your server's cache once you updated any setting</i>
                    </p>
                    <?php
                    }// if( $ct_license_status !== false && $ct_license_status == 'valid' ) {
                    ?>
                    <h3 style="margin-top:40px;">Need help?</h3>
                    <p>Plugin documentation is available at <a href="http://helpforwp.com/plugins/campaign-tracker-documentation/?utm_source=CampainTracker&utm_medium=SettingsPage&utm_campaign=Documentation" target="_blank">HelpForWP.com</a> and <a href="http://helpforwp.com/forum/?utm_source=CampainTracker&utm_medium=SettingsPage&utm_campaign=Documentation" target="_blank">support is available here</a>.</p>
                    <br />
                    <?php
                    if( $ct_license_status !== false && $ct_license_status == 'valid' ) {
                        global $_ct_messager;
                        
                        $_ct_messager->eddslum_plugin_option_page_update_center();
						
						$_ct_messager->eddslum_plugin_option_page_expiry_coming();
                    }
                    ?>
                    <p>
                    	<?php
						$nonce = wp_create_nonce( '-ct-tab-general-settings-ajax-nonce' );
						?>
                        <input type="hidden" name="nonce" id="ct_settings_general_settings_tab_ajax_nonce" value="<?php echo $nonce; ?>" />
                    </p>
                    </form>
                </section>
                <?php 
				if( $ct_license_status !== false && $ct_license_status == 'valid' ) {
					
					$custom_var_1 = $custom_var_2 = $custom_var_3 = $custom_var_4 = $custom_var_5 = $custom_var_6 = '';
					$custom_variables_settings = array();
					$plugin_settings = get_option( $this->_ct_plugin_settings_option, '' );
					if( $plugin_settings && is_array($plugin_settings) && isset($plugin_settings['custom_variables']) ){
						$custom_variables_settings = $plugin_settings['custom_variables'];
					}
					if( isset($custom_variables_settings['var_1']) && $custom_variables_settings['var_1'] ){
						$custom_var_1 = $custom_variables_settings['var_1'];
					}
					if( isset($custom_variables_settings['var_2']) && $custom_variables_settings['var_2'] ){
						$custom_var_2 = $custom_variables_settings['var_2'];
					}
					if( isset($custom_variables_settings['var_3']) && $custom_variables_settings['var_3'] ){
						$custom_var_3 = $custom_variables_settings['var_3'];
					}
					if( isset($custom_variables_settings['var_4']) && $custom_variables_settings['var_4'] ){
						$custom_var_4 = $custom_variables_settings['var_4'];
					}
					if( isset($custom_variables_settings['var_5']) && $custom_variables_settings['var_5'] ){
						$custom_var_5 = $custom_variables_settings['var_5'];
					}
					if( isset($custom_variables_settings['var_6']) && $custom_variables_settings['var_6'] ){
						$custom_var_6 = $custom_variables_settings['var_6'];
					}
				
                    $tab_action_url = add_query_arg( 'target', 'custom-variables', $action );
                    ?>
                    <section id="ct_tab_content_custom_var">
                        <form action="<?php echo $tab_action_url; ?>" method="POST" id="ct_custom_variables_form_id">
                        <h3>Custom variables</h3>
                        <p>In this section you can configure up to six custom variables that can also be passed through your website forms. You might have URL variables like this:</p>
                        <p>myvar=my-first-value&myvar2=another-value </p>
                        <p>Adding the name of the variables here will make them available when you’re mapping individual forms.</p>
                        <p>
                            <label>Custom Variable 1:</label>
                            <input type="text" name="ct_custom_var_1" value="<?php echo $custom_var_1; ?>" class="custom-var-input" />
                        </p>
                        <p>
                            <label>Custom Variable 2:</label>
                            <input type="text" name="ct_custom_var_2" value="<?php echo $custom_var_2; ?>" class="custom-var-input" />
                        </p>
                        <p>
                            <label>Custom Variable 3:</label>
                            <input type="text" name="ct_custom_var_3" value="<?php echo $custom_var_3; ?>" class="custom-var-input" />
                        </p>
                        <p>
                            <label>Custom Variable 4:</label>
                            <input type="text" name="ct_custom_var_4" value="<?php echo $custom_var_4; ?>" class="custom-var-input" />
                        </p>
                        <p>
                            <label>Custom Variable 5:</label>
                            <input type="text" name="ct_custom_var_5" value="<?php echo $custom_var_5; ?>" class="custom-var-input" />
                        </p>
                        <p>
                            <label>Custom Variable 6:</label>
                            <input type="text" name="ct_custom_var_6" value="<?php echo $custom_var_6; ?>" class="custom-var-input" />
                        </p>
                        <p>
                            <?php
                            $nonce = wp_create_nonce( '-ct-tab-custom-save-settings-' );
                            ?>
                            <input type="hidden" name="ct_action" id="ct_action_4_custom_variables_form_ID" value="" />
                            <input type="hidden" name="nonce" value="<?php echo $nonce; ?>" />
                            <input type="button" class="button-primary" value="Save Settings" id="ct_tab_custom_save_settings_btn_ID" />
                        </p>
                        </form>
                    </section>
                    <?php
					$tab_action_url = add_query_arg( 'target', 'campaign-settings', $action );
					?>
                    <section id="ct_tab_content_campaign_settings">
                        <form action="<?php echo $tab_action_url; ?>" method="POST" id="ct_custom_variables_form_id">
                        <?php $this->_ct_options_campaign_settings_OBJECT->ct_options(); ?>
                        <p>
                            <?php
                            $nonce = wp_create_nonce( '-ct-tab-campaign-settings-save-' );
                            ?>
                            <input type="hidden" name="ct_action" id="ct_action_4_campaign_settings_form_ID" value="" />
                            <input type="hidden" name="nonce" value="<?php echo $nonce; ?>" />
                        </p>
                        </form>
                    </section>
					<?php
					//Gravity Forms
                    if( is_array($this->_ct_supported_modules) && count($this->_ct_supported_modules) > 0 && 
                        isset($this->_ct_supported_modules['gravity_forms']) ){
                    ?>
                    <section>
                        <form action="<?php echo $action; ?>" method="POST" id="ct_gravity_forms_form_id">
                        <?php $this->_ct_supported_modules['gravity_forms']->ct_settings(); ?>
                        </form>
                    </section>
                    <?php
                    }
                    //Ninja forms
                    if( is_array($this->_ct_supported_modules) && count($this->_ct_supported_modules) > 0 && 
                        isset($this->_ct_supported_modules['ninja_forms']) ){
                    ?>
                    <section>
                        <form action="<?php echo $action; ?>" method="POST" id="ct_ninja_forms_form_id">
                        <?php $this->_ct_supported_modules['ninja_forms']->ct_settings(); ?>
                        </form>
                    </section>
                <?php
					}
					//Formidable forms
                    if( is_array($this->_ct_supported_modules) && count($this->_ct_supported_modules) > 0 && 
                        isset($this->_ct_supported_modules['formidable_forms']) ){
                    ?>
                    <section>
                        <form action="<?php echo $action; ?>" method="POST" id="ct_formidable_forms_form_id">
                        <?php $this->_ct_supported_modules['formidable_forms']->ct_settings(); ?>
                        </form>
                    </section>
                <?php
					}
					//Contact Form 7 forms
                    if( is_array($this->_ct_supported_modules) && count($this->_ct_supported_modules) > 0 && 
                        isset($this->_ct_supported_modules['wpcf7_contact_form']) ){
                    ?>
                    <section>
                        <form action="<?php echo $action; ?>" method="POST" id="ct_contact_form_7_forms_form_id">
                        <?php $this->_ct_supported_modules['wpcf7_contact_form']->ct_settings(); ?>
                        </form>
                    </section>
                <?php
					}
				} //end of if( $ct_license_status !== false && $ct_license_status == 'valid' ) {
				
				$target_tab = isset($_REQUEST['target']) ? $_REQUEST['target'] : '';
				?>
                <input type="hidden" id="ct_options_page_target_tab_ID" value="<?php echo $target_tab; ?>" />
            </div>
		</div>
		<?php 
	}
	
	function ct_save_custom_variables_settings_fun( $data ){
		global $current_user;
		
		if( !wp_verify_nonce( $data['nonce'], '-ct-tab-custom-save-settings-' ) ) {
			return;
		}
		if( $current_user->ID < 1 ){
			return;
		}
		
		$custom_variables_settings = array();
		$custom_variables_settings['var_1'] = trim($_POST['ct_custom_var_1']);
		$custom_variables_settings['var_2'] = trim($_POST['ct_custom_var_2']);
		$custom_variables_settings['var_3'] = trim($_POST['ct_custom_var_3']);
		$custom_variables_settings['var_4'] = trim($_POST['ct_custom_var_4']);
		$custom_variables_settings['var_5'] = trim($_POST['ct_custom_var_5']);
		$custom_variables_settings['var_6'] = trim($_POST['ct_custom_var_6']);
		
		//only allow A-Z, 0-9
		foreach( $custom_variables_settings as $key => $var ){
			$var_new = $newtext = preg_replace("/[^A-z0-9]/", "", $var);
			
			$custom_variables_settings[$key] = $var_new;
		}
		
		$plugin_settings = get_option( $this->_ct_plugin_settings_option, '' );
		if( !$plugin_settings || !is_array($plugin_settings) ){
			$plugin_settings = array();
		}
		$plugin_settings['custom_variables'] = $custom_variables_settings;

		update_option( $this->_ct_plugin_settings_option, $plugin_settings );
	}
	
	function ct_save_cookie_days_fun(){
		if( !check_ajax_referer( "-ct-tab-general-settings-ajax-nonce", 'nonce', false ) ){
			wp_die( 'ERROR: Invalid nonce val' );
		}
		if( !current_user_can('level_10') ){
			wp_die( 'ERROR: You do not have sufficient permissions to access this page.' );
		}
		
		$plugin_settings = get_option( $this->_ct_plugin_settings_option, '' );
		if( !$plugin_settings || !is_array($plugin_settings) ){
			$plugin_settings = array();
		}
		$plugin_settings['cookie_days'] = $_POST['cookiedays'];
		
		update_option( $this->_ct_plugin_settings_option, $plugin_settings );
		
		wp_die( '' );
	}
	
	function ct_save_populating_way_fun(){
		if( !check_ajax_referer( "-ct-tab-general-settings-ajax-nonce", 'nonce', false ) ){
			wp_die( 'ERROR: Invalid nonce val' );
		}
		if( !current_user_can('level_10') ){
			wp_die( 'ERROR: You do not have sufficient permissions to access this page.' );
		}
		
		$plugin_settings = get_option( $this->_ct_plugin_settings_option, '' );
		if( !$plugin_settings || !is_array($plugin_settings) ){
			$plugin_settings = array();
		}
		$plugin_settings['populating_way'] = $_POST['populatingway'];
		
		update_option( $this->_ct_plugin_settings_option, $plugin_settings );
		
		wp_die( '' );
	}
	
}
