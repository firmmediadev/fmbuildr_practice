<?php get_header(); ?>
	<main id="main">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-8">					
					<div id="content">
						<?php while ( have_posts() ) : the_post(); ?>
		                    <?php get_template_part( 'blocks/content', get_post_type() ); ?>                  
		                    <?php get_template_part( 'blocks/pager-single', get_post_type() ); ?>
		                <?php endwhile; ?>
		            </div>
				</div>
				<div class="col-md-4">
					<?php get_sidebar(); ?>					
				</div>	
			</div>    
    	</div>
	</main>
<?php get_footer(); ?>