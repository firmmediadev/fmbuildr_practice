<?php
/**
 * Plugin Name: FM Reviews Template
 * Plugin URI: https://firm-media.com
 * Description: This plugin adds a reviews template with acf fields with sorting capability. Needs FM isotope and ACF pro fields plugin to fully function
 * Version: 1.0.0
 * Author: Firm Media - Dev Team
 * Author URI: https://firm-media.com
 * License: GPL2
 */
 
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_58349468b9f0b',
	'title' => 'Review Plugin',
	'fields' => array (
		array (
			'key' => 'field_583494b937737',
			'label' => 'Reviewer Name',
			'name' => 'review_from',
			'type' => 'text',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'maxlength' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
		),
		array (
			'key' => 'field_583494dc37738',
			'label' => 'Source Url',
			'name' => 'source_url',
			'type' => 'text',
			'value' => NULL,
			'instructions' => 'URL for the source of the review',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#',
			'maxlength' => '',
			'placeholder' => 'http://socialnewsiteexample.com/reviewer1',
			'prepend' => '',
			'append' => '',
		),
		array (
			'key' => 'field_59374065629a0',
			'label' => 'Rating',
			'name' => 'rating',
			'type' => 'select',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array (
				1 => '1',
				2 => '2',
				3 => '3',
				4 => '4',
				5 => '5',
			),
			'default_value' => array (
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'ajax' => 0,
			'return_format' => 'value',
			'placeholder' => '',
		),
		array (
			'key' => 'field_5834954737739',
			'label' => 'Source / Category',
			'name' => 'source',
			'type' => 'select',
			'value' => NULL,
			'instructions' => 'Select which website the review came from',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'multiple' => 0,
			'allow_null' => 0,
			'choices' => array (
				'realself' => 'Realself',
				'facebook' => 'Facebook',
				'yelp' => 'Yelp',
				'yellowpages' => 'Yellowpages',
				'realpatientratings' => 'Real Patient Ratings',
				'zocdoc' => 'Zocdoc',
				'googlereviews' => 'Google Reviews',
				'vitals' => 'Vitals',
				'demandforce' => 'demandforce',
				'uncategorized' => 'Other',
			),
			'default_value' => array (
			),
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
			'return_format' => 'value',
		),
		array (
			'key' => 'field_5834bdea3773a',
			'label' => 'Procedure',
			'name' => 'procedure',
			'type' => 'select',
			'value' => NULL,
			'instructions' => 'Categorize the post with a procedure',
			'required' => 1,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'multiple' => 0,
			'allow_null' => 0,
			'choices' => array (
				'other' => 'Other',
				'noprocedure' => 'No Procedure Listed',
			),
			'default_value' => array (
			),
			'ui' => 0,
			'ajax' => 0,
			'placeholder' => '',
			'return_format' => 'value',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'review',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

acf_add_local_field_group(array (
	'key' => 'group_586d437af2021',
	'title' => 'Reviews Plugin Options',
	'fields' => array (
		array (
			'key' => 'field_586d5312687f6',
			'label' => 'Settings',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_586d53cc687f8',
			'label' => 'Font Color',
			'name' => 'font_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#222',
		),
		array (
			'key' => 'field_586d533a687f7',
			'label' => 'Top Margin',
			'name' => 'top_margin',
			'type' => 'text',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'maxlength' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
		),
		array (
			'key' => 'field_586d53f1687f9',
			'label' => 'Font Size',
			'name' => 'font_size',
			'type' => 'text',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'maxlength' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
		),
		array (
			'key' => 'field_586d88ba041a8',
			'label' => 'Default Site Logo',
			'name' => 'rev_site_logo',
			'type' => 'image',
			'value' => NULL,
			'instructions' => 'Default Fallback for site\'s square logo. ( uncategorized source )',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'thumbnail',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array (
			'key' => 'field_586fdaaffbba8',
			'label' => 'Sources and Procedures',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_586fd56ed44a0',
			'label' => 'Sources',
			'name' => 'sources',
			'type' => 'repeater',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'min' => 0,
			'max' => 0,
			'layout' => 'table',
			'button_label' => '',
			'collapsed' => '',
			'sub_fields' => array (
				array (
					'key' => 'field_586fd6b04a571',
					'label' => 'Value',
					'name' => 'value',
					'type' => 'text',
					'value' => NULL,
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'maxlength' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
				),
				array (
					'key' => 'field_586fd6b64a572',
					'label' => 'Label',
					'name' => 'label',
					'type' => 'text',
					'value' => NULL,
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'maxlength' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
				),
			),
		),
		array (
			'key' => 'field_586fd576d44a1',
			'label' => 'Procedures',
			'name' => 'procedures',
			'type' => 'repeater',
			'value' => NULL,
			'instructions' => 'Each entries should be one line each.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'min' => 0,
			'max' => 0,
			'layout' => 'table',
			'button_label' => '',
			'collapsed' => '',
			'sub_fields' => array (
				array (
					'key' => 'field_586fd6cc4a573',
					'label' => 'Value',
					'name' => 'value',
					'type' => 'text',
					'value' => NULL,
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'maxlength' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
				),
				array (
					'key' => 'field_586fd6d14a574',
					'label' => 'Label',
					'name' => 'label',
					'type' => 'text',
					'value' => NULL,
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'maxlength' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
				),
			),
		),
		array (
			'key' => 'field_586d47c3e8e35',
			'label' => 'Button',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_586d438949b1e',
			'label' => 'Button Font Color',
			'name' => 'button_font_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#222',
		),
		array (
			'key' => 'field_586d43ac49b1f',
			'label' => 'Button Background Color',
			'name' => 'button_background_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#eeeeee',
		),
		array (
			'key' => 'field_586d43e349b20',
			'label' => 'Button Hover Font Color',
			'name' => 'button_hover_font_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#222',
		),
		array (
			'key' => 'field_586d444e49b21',
			'label' => 'Button Hover Background Color',
			'name' => 'button_hover_background_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#99b8e1',
		),
		array (
			'key' => 'field_586d44ab49b22',
			'label' => 'Button Active Font Color',
			'name' => 'button_active_font_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#ffffff',
		),
		array (
			'key' => 'field_586d44df49b23',
			'label' => 'Button Active Background Color',
			'name' => 'button_active_background_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#43556c',
		),
		array (
			'key' => 'field_586d4821855e6',
			'label' => 'Button Mobile',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_586d464549b24',
			'label' => 'Button Mobile Font Color',
			'name' => 'button_mobile_font_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#ffffff',
		),
		array (
			'key' => 'field_586d474349b25',
			'label' => 'Button Mobile Background Color',
			'name' => 'button_mobile_background_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#8da8af',
		),
		array (
			'key' => 'field_586d475b49b26',
			'label' => 'Button Mobile Hover Font Color',
			'name' => 'button_mobile_hover_font_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#ffffff',
		),
		array (
			'key' => 'field_586d477849b27',
			'label' => 'Button Mobile Hover Background Color',
			'name' => 'button_mobile_hover_background_color',
			'type' => 'color_picker',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '#a7c7cf',
		),
		array (
			'key' => 'field_586fdaf83e9c6',
			'label' => 'Custom CSS rewrites',
			'name' => '',
			'type' => 'tab',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'placement' => 'top',
			'endpoint' => 0,
		),
		array (
			'key' => 'field_586fdac5fbba9',
			'label' => 'Custom CSS rewrites',
			'name' => 'custom_css_rewrites',
			'type' => 'textarea',
			'value' => NULL,
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'new_lines' => '',
			'maxlength' => '',
			'placeholder' => '',
			'rows' => 10,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'theme-reviews-settings',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

endif;

// FOR REVIEW PAGE TEMPLATE
add_action( 'init', 'create_posttype' );
function create_posttype() {
  register_post_type( 'review',
    array(
      'labels' => array(
        'name' => __( 'Reviews' ),
        'singular_name' => __( 'Review' ),
        'add_new' => __( 'Add New' ),
        'add_new_item' => __( 'Add New Review'),
        'edit' => __( 'Edit'),
        'edit_item' => __( 'Edit Review', 'FM-reviews-template' ),
        'new_item' => __( 'New Review', 'FM-reviews-template' ),
        'view' => __( 'View Reviews', 'FM-reviews-template' ),
        'view_item' => __( 'View Review', 'FM-reviews-template' ),
        'search_items' => __( 'Search Reviews', 'FM-reviews-template' ),
        'not_found' => __( 'No Reviews found', 'FM-reviews-template' ),
        'not_found_in_trash' => __( 'No Reviews found in trash', 'FM-reviews-template' ),
        'parent' => __( 'Parent Review', 'FM-reviews-template' ),
      ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'reviews'),
	  'description' => __( 'This is where you can create new Reviews for your site.'),
	  'public' => true,
	  'show_ui' => true,
	  'capability_type' => 'post',
	  'has_archive' => true,
	  'publicly_queryable' => true,
	  'exclude_from_search' => false,
	  'menu_position' => 20,
	  'hierarchical' => false,
	  'query_var' => true,
	  'supports' => array( 'title', 'excerpt', 'editor' , 'thumbnail'),
    )
  );

}
//END OF REVIEW PAGE TEMPLATE 
function reviews_scripts() {
	wp_enqueue_style( 'reviews-style', plugin_dir_url( __FILE__ ) . 'css/compiled/style.css', array() );
	wp_enqueue_style( 'custom-style', plugin_dir_url( __FILE__ ) . 'css/custom.css', array() );
// 	wp_enqueue_style( 'custom-foundation-style', plugin_dir_url( __FILE__ ) . 'css/compiled/foundation.min.css', array() );
	wp_enqueue_script( 'fm-isotope-js', plugins_url( 'js/fm-isotope-all.min.js', __FILE__ ) , array( 'jquery' ), 'null', true );
	//wp_enqueue_script( 'fm-isotope', plugins_url( 'js/isotope.pkgd.min.js', __FILE__ ), array('jquery'), '2.2.0', true );	
	//wp_enqueue_script( 'fm-isotope', plugins_url( 'js/isotope.pkgd.js', __FILE__ ), array( 'jquery' ), '2.2.0', true );
	//wp_enqueue_script( 'fm-isotope-js', plugins_url( 'js/fm-isotope.js', __FILE__ ) , array( 'jquery' ), 'null', true );	
	//wp_enqueue_script('custom-js', plugin_dir_url( __FILE__ ) . 'js/script.js', array( 'jquery' ),'', true );

}



add_action( 'wp_enqueue_scripts', 'reviews_scripts' );


add_filter( 'template_include', 'include_template_function', 1 );

function include_template_function( $template_path ) {
    if ( get_post_type() == 'review' ) {
        if ( is_archive() ) {
            // checks if the file exists in the theme first,
            // otherwise serve the file from the plugin
            if ( $theme_file = locate_template( array ( 'archive-review.php' ) ) ) {
                $template_path = $theme_file;
            } else {
                $template_path = plugin_dir_path( __FILE__ ) . '/archive-review.php';
            }
        }
    }
    return $template_path;
}

function limit_words($string, $word_limit) {
	$words = explode(" ", $string);
	return implode(" ",array_splice($words, 0 , $word_limit));
}

add_action( 'pre_get_posts', 'my_post_queries' );
function my_post_queries( $query ) {

    if( is_post_type_archive( 'review' ) && !is_admin() && $query->is_main_query() ) {
        $query->set( 'posts_per_page', 1000 );        
    }

}

if( function_exists('acf_add_options_page') ) {
		acf_add_options_page(array(
		'page_title' 	=> 'Reviews Plugin Settings',
		'menu_title'	=> 'Reviews Plugin',
		'menu_slug' 	=> 'theme-reviews-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false,
		'icon_url' => 'dashicons-images-alt2'
	));
}


function acf_sources_field_choices( $field ) {
    
    // reset choices
    $field['choices'] = array();

    // if has rows
    if( have_rows('sources', 'options') ) {
        
        // while has rows
        while( have_rows('sources', 'options') ) {            
            // instantiate row
            the_row();            
            // vars
            $value = get_sub_field('value');
            $label = get_sub_field('label');            
            // append to choices
            $field['choices'][ $value ] = $label;            
        }        
    }
    // return the field
    return $field;    
}

add_filter('acf/load_field/key=field_5834954737739', 'acf_sources_field_choices');


function acf_procedures_field_choices( $field ) {
    
    // reset choices
    $field['choices'] = array();

    // if has rows
    if( have_rows('procedures', 'options') ) {
        
        // while has rows
        while( have_rows('procedures', 'options') ) {            
            // instantiate row
            the_row();            
            // vars
            $value = get_sub_field('value');
            $label = get_sub_field('label');            
            // append to choices
            $field['choices'][ $value ] = $label;            
        }        
    }
    // return the field
    return $field;    
}

add_filter('acf/load_field/key=field_5834bdea3773a', 'acf_procedures_field_choices');




add_theme_support( 'post-thumbnails', array( 'post','review' ) ); // reviews 
 ?>