# FIRM MEDIA REVIEW TEMPLATE #

Author: Firm Media - Dev Team

### What is this repository for? ###

* This plugin adds a reviews template with acf fields with sorting capability. Needs FM isotope and ACF pro fields plugin to fully function
* Version 1.0
* https://firm-media.com

### How do I get set up? ###

* Install Isotope plugin, ACF Fields Pro and this plugin all together