# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Firm Media Builder 3.0

### Patch Notes ###

* Version 3.0.8 (P.C. notes) 
	* Added the Plugin "Campaign Tracker for WordPress".
	* Fixed archive excerpt not showing "Read More" link whenever the post has an empty "excerpt" field.
	* Fixed the reviews plugin bug not showing Google Reviews Icon
	* Plugins Updated
	* Added "Two Column Widget" ACF relationship field for full/2col width templates.
	* Added "Contact Form Widget" ACF relationship field for full/2col width templates.
	* Added another type of Navigation template under Customizer > Top Bar , labelled "Off Canvas Tiles (mobile)". This will toggle the navigation with four(4) tiles when it hits 0px - 420px media size. It will use the template file nav-offcanvas-tiles.php, CSS will just relate to _navigation-bar.scss around line 153 to 214 (labels added for guide).
	* Exported Database for the new Version labelled dbnewstart-ver3.0.8	


* Version 3.0.7 (J.W. notes) 
	* disabled search engine indexing in read setting of wordpress settings
	* removed second user for Joshua
	* updated the in lines styles in component-frontpage-banner.php to reflect a mobile first structure
	* updated _frontpage-banner.scss to include sass variables and styles for offsetting the frontpage banner's h1 in relation to the .sticky-wrapper-navigation height. 
	
* Version 3.0.6 (P.C. notes)
	* Added TwentyTwenty.js for easy integration
	* Fixed Datepicker css bug when using gravity forms date picker
	* Added Base Background Color (color picker) on the ACF Text Widget
	* Updated Social tab on Theme Setting to support custom images and SVG for social or website fonts. ( SVG will have a default width of 30px )
	* Added a Warning message on the loader and navigation when a user's browser doesn't have Javascript enabled. (added a link for instructions)
	* Fixed the Mobile Expander wrapper's white gradient to not interfere with the expand and collapse button.	
	* Updated local url from http://fmbuildr.local to http://local.fmbuildr.com
	* Added new DB tagged dbnewstart-ver3.0.6


* Version 3.0.5 (P.C. notes)
	* Updated the template for loop-archive-excerpt.php. Post excerpts will now show a more cleaner Header, paddings and margins
	* Changed and disabled the function in initDebugin() that shows breakpoint console logs
	* Updated _banner.scss for a better uniform breakpoint coding. notice on the backend the acf field "Page Banner Fields" will have the text field "Banner Base Height", this will serve as a default for every breakpoint if the .banner-content class name's breakpoint sizes are not filled out.
	* Further fixed the flashing bug on the navigation using a CSS/JS that waits for the site to fully load Javascript using the class name .wait-js and associating it with a function inside initJsEssentials()
	* Deleted test.txt
	* Added new DB tagged dbnewstart-ver3.0.5

* Version 3.0.4 (P.C. notes)
	* using the class .stars will generate a row of 5 font awesome stars /n
	* Added the JS function initBrowserExposure() this will detect which browser and OS the site is being viewed and adds a class name on the HTML tag. this will help us on adding browser specific CSS when we're doing cross browser compatibilities.
	* fixed navigation bug by using the .no-js class, Also added a fallback functionality by adding a class name "wait-js" with 0 opacity then removing it when the javascript is ready.
	* Plugins Updated
	* Added Widget Templates for Reviews, Galleries and Request Consultation since it's a very common widget that we use.
	* Added _widget.scss on components folder
	* Added Links for Gallery, Reviews Plugin and Contact page on the starter navigation
	* Adjusted default Style Guide CSS for xmedium -> small
	* Fixed inconsistent variables on navigation-bar.scss
	* Added new DB tagged dbnewstart-ver3.0.4


* Version 3.0.3
	* cleaned up php syntax issues that created white screens when using php 7.*
	* updated setting.scss in include google fonts on site see comments 0. Fonts also commented out style_enqueue for google fonts so we can toggle this back on if needed.
	* replaced /plugins/kraken.io with wpsmushpro (better image compression)
	* added intuitive post-type plugin 
	* added expand-collapse layout button to mce fuctions
	
* Version 3.0.2
	* Fixed the bug that makes the mobile nav bar to have an extra space whenever that off-canvas menu open.
	* Added more scaffolding feature for front page's flexible field.
	* Updated Parallax Feature
	* Added Custom post type (procedure & gallery)
	* Installed the main plugins from the last builder
	
* Version 3.0.1
	* Implemented Flexible Fields on the Homepage (scaffolding)
	* Added Image/Video Banners Templates for the front page and sub pages
	* Started Parallax feature
	* Updated Settings.scss

* Version 3.0.0
	* Added Foundation XY grid
	* Added Flickity
	* Added Fancybox
	* Downloaded a fresh WP-JOINT
	* Added front-page.php
	* Added home.php


### How do I get set up? ###

* Summary of set up
* Configuration
	-- Run npm install when pulled locally
* Dependencies 
	-- Gulp
	-- Flickity
	-- Fancybox 3
* Database configuration
	-- Database is location: wp-content/db_bu/	
	-- With WP-CLI : Run -> wp db import db_bu/NAMEOFDATABASE.sql
	-- Use WP-CLI : Run -> wp search-replace "String that wants to be change" "string to replace it" 
* How to run tests
	-- Do : npm run browsersync

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Firm Media : webmaster@firm-media.com
